//
//  PatientInfoCell.swift
//  TotalHealthDental
//
//  Created by Leojin Bose on 4/17/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

protocol  PatientInfoCellDelegate {
    func radioButtonTappedForCell(cell: PatientInfoCell)
}

class PatientInfoCell: UITableViewCell {

    @IBOutlet var labelQuestion: UILabel!
    @IBOutlet var radioButtonYes: RadioButton!
    @IBOutlet var radioButtonNo: RadioButton!
    
    var question: MCQuestion!
    var delegate: PatientInfoCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configCell(question: MCQuestion) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        self.question = question
        if(self.question.selectedOption == nil) {
            self.radioButtonYes.deselectAllButtons()
        } else {
            self.radioButtonYes.setSelected(self.question.selectedOption == true)
        }
//        if arrayHeading.contains(self.question.question){
//            self.labelQuestion.font = UIFont(name: "WorkSans-Medium", size: 20.0)
//            let string : NSString = self.question.question
//            let range = string.rangeOfString(self.question.question)
//            let attributedString = NSMutableAttributedString(string: string as String)
//            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: range)
//            self.labelQuestion.attributedText = attributedString
//
//            self.radioButtonYes.hidden = true
//            self.radioButtonNo.hidden = true
//        }else{
//            self.labelQuestion.font = UIFont(name: "WorkSans-Regular", size: 16.0)
//            self.radioButtonYes.hidden = false
//            self.radioButtonNo.hidden = false
            self.labelQuestion.text = self.question.question

//        }
    }
    
    func configCellWithAsterix(question: MCQuestion) {
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        self.question = question
        if(self.question.selectedOption == nil) {
            self.radioButtonYes.deselectAllButtons()
        } else {
            self.radioButtonYes.setSelected(self.question.selectedOption == true)
        }
        self.labelQuestion.text = self.question.question + " *"
    }
    
    @IBAction func radioButtonPressed(sender: RadioButton) {
        self.question.selectedOption = sender == radioButtonYes
        if question.isAnswerRequired == true && self.question.selectedOption == true {
            self.delegate.radioButtonTappedForCell(cell: self)
        }
    }
}
