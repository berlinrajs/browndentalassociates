//
//  Dentures1ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/18/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class Dentures1ViewController: MCViewController {

    @IBOutlet weak var labelDetails : UILabel!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var signatureDentist : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        var string : NSString = labelDetails.text! as NSString
        string = string.replacingOccurrences(of: "KDOCTORNAME", with: patient.dentistName) as NSString
        let range = string.range(of: patient.dentistName)
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        let range1 = string.range(of: "INFORMED CONSENT:")
        attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 16), range: range1)
        labelDetails.attributedText = attributedString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() || !signatureDentist.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let dentures = consentStoryBoard.instantiateViewController(withIdentifier: "DenturesFormVC") as! DenturesFormViewController
            dentures.patient = self.patient
            dentures.signPatient = signaturePatient.signatureImage()
            dentures.signDentist = signatureDentist.signatureImage()
            self.navigationController?.pushViewController(dentures, animated: true)
        }
    }


}
