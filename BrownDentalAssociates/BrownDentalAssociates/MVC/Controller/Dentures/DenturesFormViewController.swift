//
//  DenturesFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/18/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class DenturesFormViewController: MCViewController {

    var signPatient : UIImage!
    var signDentist : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDetails : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureDentist : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        signaturePatient.image = signPatient
        signatureDentist.image = signDentist
        var string : NSString = labelDetails.text! as NSString
        string = string.replacingOccurrences(of: "KDOCTORNAME", with: patient.dentistName) as NSString
        let range = string.range(of: patient.dentistName)
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        let range1 = string.range(of: "INFORMED CONSENT:")
        attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 16), range: range1)
        labelDetails.attributedText = attributedString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    


}
