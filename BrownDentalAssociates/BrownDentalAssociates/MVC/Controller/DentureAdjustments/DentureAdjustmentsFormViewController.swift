//
//  DentureAdjustmentsFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class DentureAdjustmentsFormViewController: MCViewController {

    var signPatient : UIImage!
    var signWitness : UIImage!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureWitness : UIImageView!
    @IBOutlet weak var labelDetails : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        signaturePatient.image = signPatient
        signatureWitness.image = signWitness
        var string : NSString = labelDetails.text! as NSString
        string = string.replacingOccurrences(of: "KDOCTORNAME", with: patient.dentistName) as NSString
        string = string.replacingOccurrences(of: "KDOCTORNAME", with: patient.dentistName) as NSString
        string = string.replacingOccurrences(of: "KPATIENTNAME", with: patient.fullName) as NSString
        let range = string.range(of: patient.fullName)
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        labelDetails.attributedText = attributedString

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
