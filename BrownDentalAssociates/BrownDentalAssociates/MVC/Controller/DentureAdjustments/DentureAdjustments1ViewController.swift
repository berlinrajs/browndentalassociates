//
//  DentureAdjustments1ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class DentureAdjustments1ViewController: MCViewController {

    @IBOutlet weak var labelDetails : UILabel!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var signatureWitness : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelDate1.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        var string : NSString = labelDetails.text! as NSString
        string = string.replacingOccurrences(of: "KDOCTORNAME", with: patient.dentistName) as NSString
        string = string.replacingOccurrences(of: "KDOCTORNAME", with: patient.dentistName) as NSString
        string = string.replacingOccurrences(of: "KPATIENTNAME", with: patient.fullName) as NSString
        let range = string.range(of: patient.fullName)
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        labelDetails.attributedText = attributedString
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
       if !signaturePatient.isSigned() || !signatureWitness.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped || !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let denture = consentStoryBoard.instantiateViewController(withIdentifier: "DentureAdjustmentsFormVC") as! DentureAdjustmentsFormViewController
            denture.patient = self.patient
            denture.signPatient = signaturePatient.signatureImage()
            denture.signWitness = signatureWitness.signatureImage()
            self.navigationController?.pushViewController(denture, animated: true)
            
        }
    }
    


}
