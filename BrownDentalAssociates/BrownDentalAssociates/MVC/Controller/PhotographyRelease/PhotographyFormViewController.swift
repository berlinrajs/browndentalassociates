//
//  PhotographyFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/17/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class PhotographyFormViewController: MCViewController {

    var signPatient : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelPatientName1 : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelRelationship : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelPatientName1.text = patient.fullName
        labelDate1.text = patient.dateToday
        labelRelationship.text = patient.photographyRelation
        signaturePatient.image = signPatient

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
