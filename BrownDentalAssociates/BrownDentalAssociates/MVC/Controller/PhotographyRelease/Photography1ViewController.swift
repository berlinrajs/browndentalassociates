//
//  Photography1ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/17/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class Photography1ViewController: MCViewController {

    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var textfieldRelationship : MCTextField!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        labelDate.todayDate = patient.dateToday
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue() {
        textfieldRelationship.text = patient.photographyRelation
    }
    
    func saveValue (){
        patient.photographyRelation = textfieldRelationship.isEmpty ? "" : textfieldRelationship.text!
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()

    }

    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let photo = consentStoryBoard.instantiateViewController(withIdentifier: "PhotographyFormVC") as! PhotographyFormViewController
            photo.patient = self.patient
            photo.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(photo, animated: true)

        }
    }



}
