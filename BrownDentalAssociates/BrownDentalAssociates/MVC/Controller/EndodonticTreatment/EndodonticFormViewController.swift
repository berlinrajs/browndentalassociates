//
//  EndodonticFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/17/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class EndodonticFormViewController: MCViewController {

    var signPatient : UIImage!
    var signDentist : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelToothNumber : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var signatureDentist : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelToothNumber.text = "TOOTH NAME/NUMBER : " + patient.selectedForms.first!.toothNumbers
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        signaturePatient.image = signPatient
        signatureDentist.image = signDentist
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
