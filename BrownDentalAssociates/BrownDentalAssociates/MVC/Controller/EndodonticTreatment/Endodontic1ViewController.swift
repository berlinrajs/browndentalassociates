//
//  Endodontic1ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/17/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class Endodontic1ViewController: MCViewController {

    @IBOutlet weak var labelToothNumber : UILabel!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var signatureDentist : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelToothNumber.text = "TOOTH NAME/NUMBER : " + patient.selectedForms.first!.toothNumbers

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() || !signatureDentist.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let endo = consentStoryBoard.instantiateViewController(withIdentifier: "EndodonticFormVC") as! EndodonticFormViewController
            endo.patient = self.patient
            endo.signPatient = signaturePatient.signatureImage()
            endo.signDentist = signatureDentist.signatureImage()
            self.navigationController?.pushViewController(endo, animated: true)
        }
    }


}
