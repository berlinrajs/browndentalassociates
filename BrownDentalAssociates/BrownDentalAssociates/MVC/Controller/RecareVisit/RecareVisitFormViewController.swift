//
//  RecareVisitFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class RecareVisitFormViewController: MCViewController {

    var signPatient : UIImage!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelLastName : UILabel!
    @IBOutlet weak var labelFirstName : UILabel!
    @IBOutlet weak var labelInitial : UILabel!
    @IBOutlet weak var labelPreferredName : UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate1.text = patient.dateToday
        signaturePatient.image = signPatient
        labelLastName.text = patient.lastName
        labelFirstName.text = patient.firstName
        labelInitial.text = patient.initial
        labelPreferredName.text = patient.preferredName

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
