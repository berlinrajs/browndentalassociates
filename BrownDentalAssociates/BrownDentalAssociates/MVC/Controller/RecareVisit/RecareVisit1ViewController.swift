//
//  RecareVisit1ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class RecareVisit1ViewController: MCViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped  {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let recare = consentStoryBoard.instantiateViewController(withIdentifier: "RecareVisitFormVC") as! RecareVisitFormViewController
            recare.patient = self.patient
            recare.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(recare, animated: true)
        }
    }
}
