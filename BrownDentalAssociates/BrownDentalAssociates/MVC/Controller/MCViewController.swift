//
//  MCViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


let mainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
let consentStoryBoard = UIStoryboard(name: "Consent", bundle: Bundle.main)
let patientStoryBoard = UIStoryboard(name: "NewPatient", bundle: Bundle.main)
let medicalStoryBoard = UIStoryboard(name: "MedicalHistory", bundle: Bundle.main)

class MCViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: MCButton?
    @IBOutlet var buttonBack: MCButton?
    
    @IBOutlet var pdfView: UIScrollView?
    var completion: ((_ success: Bool) -> Void)?
    var patient: MCPatient!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = UIRectEdge()
        
        configureNavigationButtons()
        // Do any additional setup after loading the view.
    }
    
    func configureNavigationButtons() {
        self.buttonSubmit?.backgroundColor = UIColor.green
        self.buttonBack?.isHidden = self.buttonSubmit == nil && isFromPreviousForm
        
        if self.buttonBack != nil && self.buttonSubmit == nil && isFromPreviousForm {
            self.navigationController?.viewControllers.removeSubrange(1...self.navigationController!.viewControllers.index(of: self)! - 1)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var isFromPreviousForm: Bool {
        get {
            return navigationController!.viewControllers.count > 3 ? true : false
        }
    }
    
    @IBAction func buttonBackAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmitButtonPressed() {
        
        if !Reachability.isConnectedToNetwork() {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 0 {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                } else {
                    
                }
            })
            return
        }
        uploadPdfToDrive()
    }
    
    func sendFormToGoogleDrive(completion: @escaping (_ success: Bool) -> Void) {
        self.uploadPdfToDrive()
    }
    
    func sendFormToServer() {
        if !Reachability.isConnectedToNetwork() {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 1 {
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                } else {
                    
                }
            })
            return
        }
        var image: UIImage?
        self.buttonSubmit?.isHidden = true
        self.buttonBack?.isHidden = true
        self.pdfView?.isScrollEnabled = false
        self.pdfView?.clipsToBounds = false
        let size: CGSize = CGSize(width: self.pdfView!.contentSize.width, height: self.pdfView!.contentSize.height)
        
        let savedContentOffset = self.pdfView!.contentOffset
        UIGraphicsBeginImageContext(size)
        self.pdfView?.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        image = UIGraphicsGetImageFromCurrentImageContext()
        self.pdfView?.contentOffset = savedContentOffset
        UIGraphicsEndImageContext()
        
        func resetValues() {
            self.buttonSubmit?.isHidden = false
            self.buttonBack?.isHidden = false
            self.pdfView?.isScrollEnabled = true
            self.pdfView?.clipsToBounds = true
            BRProgressHUD.hide()
        }
        
        
        if let imageData = image {
            BRProgressHUD.show()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let birthDate = dateFormatter.date(from: patient.dateOfBirth)
            dateFormatter.dateFormat = "yyyy'_'MM'_'dd"
            let birthDateString = dateFormatter.string(from: birthDate!)
            
            
            dateFormatter.dateFormat = "MM'_'dd'_'yyyy"
            let dateString = dateFormatter.string(from: Date()).uppercased()
            dateFormatter.dateFormat = "MM'-'dd'-'yyyy"
            
            #if AUTO
                let params = ["first_name": patient.firstName, "last_name": patient.lastName, "doc_date": dateString, "form_name": self.patient.selectedForms.first!.formTitle.fileName, "client_id": kAppKey, "file_name": self.patient.selectedForms.first!.formTitle.fileName, "patient_id": patient.patientDetails!.patientNumber, "dob": birthDateString]
            #else
                let params = ["first_name": patient.firstName, "last_name": patient.lastName, "doc_date": dateString, "form_name": self.patient.selectedForms.first!.formTitle.fileName, "client_id": kAppKey, "file_name": self.patient.selectedForms.first!.formTitle.fileName]
            #endif
            
            let manager = AFHTTPSessionManager(baseURL: URL(string: serverPath))
            manager.responseSerializer.acceptableContentTypes = ["text/html"]
            
            //http://opcommunity.mncell.com/formreviewapp/upload_medical_history_form.php?first_name=&last_name=&doc_date=&form_name=&client_id=&file_name=
            
            manager.post("upload_medical_history_form.php", parameters: params, constructingBodyWith: { (data) in
                dateFormatter.dateFormat = "MMddyyyyHHmmSSS"
                let randomString = dateFormatter.string(from: Date()).uppercased()
                let fileName = "\(self.patient.fullName.fileName)_\(dateString)_\(self.patient.selectedForms.first!.formTitle.fileName)_\(randomString).jpg"
                data.appendPart(withFileData: UIImageJPEGRepresentation(imageData, 1.0)!, name: self.patient.selectedForms.first!.formTitle.fileName, fileName: fileName, mimeType: "image/jpeg")
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                resetValues()
                self.patient.selectedForms.removeFirst()
                self.gotoNextForm()
            }) { (task, error) in
                resetValues()
                self.showAlert(error.localizedDescription)
            }
        } else {
            self.showAlert("SOMETHIG WENT WRONG\nPLEASE TRY AGAIN")
            resetValues()
        }
    }
    
    func uploadPdfToDrive (){
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self) { (success) -> Void in
            if success {
                self.buttonSubmit?.isHidden = true
                self.buttonBack?.isHidden = true
                
                pdfManager.uploadToGoogleDrive(self.pdfView == nil ? self.view : self.pdfView!, patient: self.patient, completionBlock: { (finished) -> Void in
                    if finished {
                        if let _ = self.completion {
                            self.completion!(true)
                        } else {
                            self.patient.selectedForms.removeFirst()
                            self.gotoNextForm()
                        }
                    } else {
                        self.buttonSubmit?.isHidden = false
                        self.buttonBack?.isHidden = false
                    }
                })
            } else {
                self.buttonSubmit?.isHidden = false
                self.buttonBack?.isHidden = false
            }
        }
    }

    
    
    func gotoNextForm() {
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        
        if formNames.contains(kNewPatientSignInForm){
            let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient1VC") as! NewPatient1ViewController
            patient.patient = self.patient
            self.navigationController?.pushViewController(patient, animated: true)
        }else if formNames.contains(kMedicalHistory){
            let medical = medicalStoryBoard.instantiateViewController(withIdentifier: "MedicalHistory1VC") as! MedicalHistory1ViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
        }else  if formNames.contains(kInsuranceCard) {
            let insuranceCard = mainStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            insuranceCard.patient = self.patient
            self.navigationController?.pushViewController(insuranceCard, animated: true)
        } else if formNames.contains(kDrivingLicense) {
            let drivingLicense = mainStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            drivingLicense.patient = self.patient
            drivingLicense.isDrivingLicense = true
            self.navigationController?.pushViewController(drivingLicense, animated: true)
        }else if formNames.contains(kEndodontic){
            let endo = consentStoryBoard.instantiateViewController(withIdentifier: "Endodontic1VC") as! Endodontic1ViewController
            endo.patient = self.patient
            self.navigationController?.pushViewController(endo, animated: true)
        }else if formNames.contains(kXrayRefusal){
            let xray = consentStoryBoard.instantiateViewController(withIdentifier: "XrayRefusal1VC") as! XrayRefusal1ViewController
            xray.patient = self.patient
            self.navigationController?.pushViewController(xray, animated: true)
        }else if formNames.contains(kFinancialPolicy){
            let finance = consentStoryBoard.instantiateViewController(withIdentifier: "FinancialPolicy1VC") as! FinancialPolicy1ViewController
            finance.patient = self.patient
            self.navigationController?.pushViewController(finance, animated: true)
        }else if formNames.contains(kWhitening){
            let whitening = consentStoryBoard.instantiateViewController(withIdentifier: "Whitening1VC") as! Whitening1ViewController
            whitening.patient = self.patient
            self.navigationController?.pushViewController(whitening, animated: true)
        }else if formNames.contains(kCrownAndBridges){
            let crown = consentStoryBoard.instantiateViewController(withIdentifier: "Crown1VC") as! Crown1ViewController
            crown.patient = self.patient
            self.navigationController?.pushViewController(crown, animated: true)
        }else if formNames.contains(kPhotography){
            let photo = consentStoryBoard.instantiateViewController(withIdentifier: "Photography1VC") as! Photography1ViewController
            photo.patient = self.patient
            self.navigationController?.pushViewController(photo, animated: true)
        }else if formNames.contains(kDentures){
            let denture = consentStoryBoard.instantiateViewController(withIdentifier: "Dentures1VC") as! Dentures1ViewController
            denture.patient = self.patient
            self.navigationController?.pushViewController(denture, animated: true)
        }else if formNames.contains(kNitrousOxide){
            let nitrous = consentStoryBoard.instantiateViewController(withIdentifier: "NitrousOxide1VC") as! NitrousOxide1ViewController
            nitrous.patient = self.patient
            self.navigationController?.pushViewController(nitrous, animated: true)
        }else if formNames.contains(kSurgery){
            let surgery = consentStoryBoard.instantiateViewController(withIdentifier: "Surgery1VC") as! Surgery1ViewController
            surgery.patient = self.patient
            self.navigationController?.pushViewController(surgery, animated: true)
        }else if formNames.contains(kTreatmentFillings){
            let filling = consentStoryBoard.instantiateViewController(withIdentifier: "Fillings1VC") as! Fillings1ViewController
            filling.patient = self.patient
            self.navigationController?.pushViewController(filling, animated: true)
        }else if formNames.contains(kPeriodontitis){
            let periodontitis = consentStoryBoard.instantiateViewController(withIdentifier: "Periodontitis1VC") as! Periodontitis1ViewController
            periodontitis.patient = self.patient
            self.navigationController?.pushViewController(periodontitis, animated: true)
        }else if formNames.contains(kDentureAdjustments){
            let denture = consentStoryBoard.instantiateViewController(withIdentifier: "DentureAdjustments1VC") as! DentureAdjustments1ViewController
            denture.patient = self.patient
            self.navigationController?.pushViewController(denture, animated: true)
        }else if formNames.contains(kRecareVisit){
            let recare = consentStoryBoard.instantiateViewController(withIdentifier: "RecareVisit1VC") as! RecareVisit1ViewController
            recare.patient = self.patient
            self.navigationController?.pushViewController(recare, animated: true)
        } else if formNames.contains(kOralScreening) {
            let oralScreening = consentStoryBoard.instantiateViewController(withIdentifier: "kOralScreeningVC") as! OralScreeningViewController
            oralScreening.patient = patient
            self.navigationController?.pushViewController(oralScreening, animated: true)
            
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
            _ = self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
