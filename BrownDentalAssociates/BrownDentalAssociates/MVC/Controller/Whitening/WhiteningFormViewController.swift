//
//  WhiteningFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/17/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class WhiteningFormViewController: MCViewController {

    var signPatient : UIImage!
    @IBOutlet weak var labelName : UILabel!
    @IBOutlet weak var labelDOB : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = patient.fullName
        labelDOB.text = patient.dateOfBirth
        signaturePatient.image = signPatient
        labelDate.text = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
