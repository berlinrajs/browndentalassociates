//
//  Surgery1ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/18/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class Surgery1ViewController: MCViewController {

    @IBOutlet weak var textfieldProcedure : MCTextField!
    @IBOutlet      var buttonAnesthetic : [UIButton]!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDetails : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        var string : NSString = labelDetails.text! as NSString
        string = string.replacingOccurrences(of: "KDOCTORNAME", with: patient.dentistName) as NSString
        let range = string.range(of: patient.dentistName)
        let attributedString = NSMutableAttributedString(string: string as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        labelDetails.attributedText = attributedString

        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldProcedure.text = patient.surgeryProcedures
        for btn in buttonAnesthetic{
            btn.isSelected = patient.surgeryAnestheticTag.contains(btn.tag)
        }
    }
    
    func saveValue (){
        patient.surgeryProcedures = textfieldProcedure.isEmpty ? "" : textfieldProcedure.text!
    }

    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
        
    }
    
    @IBAction func onAnestheticButtonPressed (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.surgeryAnestheticTag.contains(sender.tag){
            patient.surgeryAnestheticTag.remove(at: patient.surgeryAnestheticTag.index(of: sender.tag)!)
        }else{
            patient.surgeryAnestheticTag.append(sender.tag)
        }
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldProcedure.isEmpty || patient.surgeryAnestheticTag.count == 0{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            let surgery = consentStoryBoard.instantiateViewController(withIdentifier: "SurgeryFormVC") as! SurgeryFormViewController
            surgery.patient = self.patient
            surgery.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(surgery, animated: true)
            
        }
    }
    


}
