//
//  SurgeryFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/18/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class SurgeryFormViewController: MCViewController {

    var signPatient : UIImage!
    @IBOutlet weak var labelLastName : UILabel!
    @IBOutlet weak var labelFirstName : UILabel!
    @IBOutlet weak var labelInitial : UILabel!
    @IBOutlet weak var labelPreferredName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelProcedure : UILabel!
    @IBOutlet      var buttonDoctors : [UIButton]!
    @IBOutlet      var buttonAnesthetic : [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelLastName.text = patient.lastName
        labelFirstName.text = patient.firstName
        labelInitial.text = patient.initial
        labelPreferredName.text = patient.preferredName
        signaturePatient.image = signPatient
        labelDate.text = patient.dateToday
        labelProcedure.text = patient.surgeryProcedures
        for btn in buttonAnesthetic{
            btn.isSelected = patient.surgeryAnestheticTag.contains(btn.tag)
        }
        for btn in buttonDoctors{
            btn.isSelected = " " + patient.dentistName == btn.titleLabel!.text!.uppercased()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
