//
//  NitrousOxide1ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/18/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NitrousOxide1ViewController: MCViewController {

    @IBOutlet weak var signaturePatient  : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate.todayDate = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let nitrous = consentStoryBoard.instantiateViewController(withIdentifier: "NitrousOxideFormVC") as! NitrousOxideFormViewController
            nitrous.patient = self.patient
            nitrous.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(nitrous, animated: true)
        }
    }



}
