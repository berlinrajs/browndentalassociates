//
//  NitrousOxideFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/18/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NitrousOxideFormViewController: MCViewController {

    var signPatient : UIImage!
    @IBOutlet weak var labelLastName : UILabel!
    @IBOutlet weak var labelFirstName : UILabel!
    @IBOutlet weak var labelInitial : UILabel!
    @IBOutlet weak var labelPreferredName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelLastName.text = patient.lastName
        labelFirstName.text = patient.firstName
        labelInitial.text = patient.initial
        labelPreferredName.text = patient.preferredName
        signaturePatient.image = signPatient
        labelDate.text = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
