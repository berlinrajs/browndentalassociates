//
//  ViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomeViewController: MCViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var tableViewForms: UITableView!
    @IBOutlet weak var labelVersion : UILabel!
    @IBOutlet weak var labelPlace: UILabel!
    
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.showCompletionAlert), name: NSNotification.Name(rawValue: kFormsCompletedNotification), object: nil)
        
        if let text = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        
        labelPlace.text = kPlace
        NotificationCenter.default.addObserver(self, selector: #selector(dateChangedNotification), name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)
        self.dateChangedNotification()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = kCommonDateFormat
        labelDate.text = dateFormatter.string(from: NSDate() as Date).uppercased()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if kAppLoginAvailable {
            loginValidation()
        }
        #if AUTO
            self.getAccessToken()
        #endif
        
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            self.formList = forms
            self.tableViewForms.reloadData()
            if isConnectionFailed == true {
                self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                    if buttonIndex == 0 {
                        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                        if let url = settingsUrl {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url, options: [:], completionHandler: { (finished) in
                                    
                                })
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                    } else {
                        
                    }
                })
            }
        }
    }
    
    #if AUTO
    func getAccessToken() {
        let params = ["userName": "srssoftwares", "password": "srssoftwares", "grant_type": "password"]
        ServiceManager.postDataToServer(serviceName: "token", parameters: params, success: { (result) in
            
        }) { (error) in
            
        }
    }
    
    @IBAction func buttonActionChangeIP(_ sender: Any) {
        PopupTextField.popUpView().showInViewController(self, WithTitle: "PASSWORD", placeHolder: "ENTER PASSWORD", textFormat: .SecureText) { (popup, textField1) in
            popup.close()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                
                if textField1.text == UserDefaults.standard.value(forKey: kAppLoginPasswordKey) as? String {
                    let popupView = PopupTextField.popUpView()
                    popupView.showInViewController(self, WithTitle: "CHANGE IP ADDRESS", placeHolder: "IP ADDRESS", textFormat: .Default) { (popupView, textField) in
                        popupView.close()
                        if !textField.isEmpty {
                            let myText = textField.text
                            UserDefaults.standard.set(myText, forKey: "hostIP")
                            UserDefaults.standard.synchronize()
                            self.getAccessToken()
                        }
                    }
                    popupView.textField.text = UserDefaults.standard.value(forKey: "hostIP") as? String
                } else {
                    self.showAlert("INCORRECT PASSWORD")
                }
            })
        }
    }
    #endif
    
    func loginValidation() {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            let defaults = UserDefaults.standard
            ServiceManager.loginWithUsername(defaults.value(forKey: kAppLoginUsernameKey) as! String, password: defaults.value(forKey: kAppLoginPasswordKey) as! String) { (success, error) -> (Void) in
                if success {
                    
                } else {
                    if error == nil {
                        
                    } else {
                        DispatchQueue.main.async(execute: {
                            UserDefaults.standard.set(false, forKey: "kApploggedIn")
                            UserDefaults.standard.synchronize()
                            (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                        })
                    }
                }
            }
        }
    }
    
    @IBAction func buttonActionNext(WithSender sender: AnyObject) {
        self.view.endEditing(true)
        selectedForms.removeAll()
//        for (_, form) in formList.enumerated() {
//            if form.isSelected == true {
//                if form.formTitle == kConsentForms  {
//                    for subForm in form.subForms {
//                        if subForm.isSelected == true {
//                            selectedForms.append(subForm)
//                        }
//                    }
//                } else {
//                    selectedForms.append(form)
//                }
//            }
//        }
        
        for (_, form) in formList.enumerated() {
            if form.isSelected == true {
                if form.subForms != nil && form.subForms.count > 0 {
                    for subForm in form.subForms {
                        if subForm.isSelected == true {
                            selectedForms.append(subForm)
                        }
                    }
                } else {
                    selectedForms.append(form)
                }
            }
        }
        
        let patient = MCPatient(forms: selectedForms)
        patient.dateToday = labelDate.text
//        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
 
//        if formNames.contains(kVisitorCheckForm) && selectedForms.count > 0 {
//            let visitorCheckIn = self.storyboard?.instantiateViewController(withIdentifier: "kVisitorVC") as! VisitorCheckinVC
//            visitorCheckIn.patient = patient
//            self.navigationController?.pushViewController(visitorCheckIn, animated: true)
//        } else if selectedForms.count == 1 && selectedForms.first!.formTitle == kFeedBack {
//            let customerReview = self.storyboard?.instantiateViewController(withIdentifier: "kFeedBackInfoViewController") as! FeedBackInfoViewController
//            customerReview.patient = patient
//            self.navigationController?.pushViewController(customerReview, animated: true)
//        } else
        if selectedForms.count > 0 {
            let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientInfoVC") as! PatientInfoViewController
            patientInfoVC.patient = patient
            self.navigationController?.pushViewController(patientInfoVC, animated: true)
        } else {
            self.showAlert("PLEASE SELECT ANY FORM")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    func showCompletionAlert() {
        self.showCustomAlert("PLEASE HANDOVER THE DEVICE BACK TO FRONT DESK")
    }
    
    @IBAction func buttonActionHistory() {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "kLoginViewController") as! LoginViewController
        loginVC.isStaffLogin = true
        let navVC = UINavigationController(rootViewController: loginVC)
        navVC.setNavigationBarHidden(true, animated: true)
        self.present(navVC, animated: true, completion: nil)
    }
}



#if AUTO
    extension HomeViewController : UITableViewDelegate {
        
        func tapGestureDidSelectAction(sender : UITapGestureRecognizer) {
            let indexPath = (sender.view as! FormsTableViewCell).indexPath!
            let form = formList[indexPath.section].subForms[indexPath.row]
            
            if indexPath.section == 0 {
                form.isSelected = !form.isSelected
                //                let newPatientForms = formList[0].subForms!.filter({ (form) -> Bool in
                //                    return (form.formTitle == kMedicalHistory || form.formTitle == kMedicalHistorySpan)
                //                }).filter({ (form) -> Bool in
                //                    return form.isSelected == true
                //                })
                var indexPaths : [IndexPath] = [IndexPath]()
                for (idx, _) in formList[0].subForms.enumerated() {
                    let indexPath = IndexPath(row: idx, section: 0)
                    indexPaths.append(indexPath)
                }
                
        
                
                //                if newPatientForms.count == 0 {
                //                    for subForm in formList[0].subForms {
                //                        subForm.isSelected = false
                //                    }
                //                }
                tableViewForms.reloadRows(at: indexPaths, with: UITableViewRowAnimation.none)
            } else {
                form.isSelected = !form.isSelected
                tableViewForms.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            }
            if form.isSelected == true && form.isToothNumberRequired == true {
              
                    PopupTextField.popUpView().showInViewController(self, WithTitle: "ENTER TOOTH NUMBERS", placeHolder: "01, 02, 03, 04", textFormat:.AlphaNumeric, completion: { (popUpView, textField) in
                        if textField.isEmpty{
                            form.isSelected = false
                            self.tableViewForms.reloadData()
                        }else{
                            form.toothNumbers = textField.text!
                        }
                        popUpView.removeFromSuperview()
                    })
                }
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm") as! FormsTableViewCell
            let form = formList[indexPath.section].subForms[indexPath.row]
            let height = form.formTitle.heightWithConstrainedWidth(472.0, font: cell.labelFormName.font) + 20
            return height
        }
        
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 102.0
        }
        
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let form = formList[section]
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! FormsTableViewCell
            let headerView = cell.contentView.subviews[1] as! PDTableHeaderView
            headerView.labelFormName.text = form.formTitle
            
            let selectedForm = formList.filter { (obj) -> Bool in
                return obj.isSelected == true
            }
            let delayTime = DispatchTime.now() + 0.2
            DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                if selectedForm.count > 0 {
                    UIView.animate(withDuration: 0.3, animations: {
                        cell.contentView.alpha = form.isSelected == false ? 0.2 : 1.0
                    })
                } else {
                    UIView.animate(withDuration: 0.3, animations: {
                        cell.contentView.alpha = 1.0
                    })
                }
            })
    
            
            headerView.tag = section
            if headerView.gestureRecognizers == nil {
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
                headerView.addGestureRecognizer(tapGesture)
            }
            return cell.contentView
        }
        
        func tapGestureAction(sender : UITapGestureRecognizer) {
            let headerView = sender.view as! PDTableHeaderView
            let form = self.formList[headerView.tag]
            
            let selectedForm = formList.filter { (obj) -> Bool in
                return obj.isSelected == true
            }
   
            func displayRows() {
                self.view.layoutIfNeeded()
                //                constraintTableViewTop.constant = 30
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (finished) in
                    //                    self.constraintTableViewBottom.constant = 30
                    //                    self.isNewPatient = form.formTitle == kNewPatient.keys.first ? true : false
                    form.isSelected = !form.isSelected
                    
                            for obj in self.formList {
                                for subForm in obj.subForms {
                                    subForm.isSelected = false
                                    }
                                }

                    
                    var indexPaths : [IndexPath] = [IndexPath]()
                    for (idx, _) in form.subForms.enumerated() {
                        let indexPath = IndexPath(row: idx, section: headerView.tag)
                        indexPaths.append(indexPath)
                    }
                    
                    self.tableViewForms.beginUpdates()
                    self.tableViewForms.insertRows(at: indexPaths, with: UITableViewRowAnimation.fade)
                    self.tableViewForms.endUpdates()
                    let delayTime = DispatchTime.now() + 0.1
                    DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                        self.tableViewForms.scrollToRow(at: indexPaths.last!, at: UITableViewScrollPosition.bottom, animated: true)
                        
                        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                            self.tableViewForms.isUserInteractionEnabled = true
                           
                            self.tableViewForms.reloadData()
                        })
                    })
                })
            }
            
            self.tableViewForms.isUserInteractionEnabled = false
            if selectedForm.count > 0 {
                selectedForm[0].isSelected = false
                let section = selectedForm[0].formTitle == kExistingPatient.keys.first ? 0 : 1
                var indexPaths : [IndexPath] = [IndexPath]()
                
                for (idx, _) in selectedForm[0].subForms.enumerated() {
                    let indexPath = IndexPath(row: idx, section: section)
                    indexPaths.append(indexPath)
                }
                //                if form.formTitle == kNewPatient.keys.first {
                //                    for obj in self.formList {
                //                        for subForm in obj.subForms {
                //                            subForm.isSelected = false
                //                        }
                //                    }
                //                }
                
                
                self.tableViewForms.beginUpdates()
                self.tableViewForms.deleteRows(at: indexPaths, with: .none)
                self.tableViewForms.endUpdates()
                
                self.view.layoutIfNeeded()
                //                constraintTableViewTop.constant = 100
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                }, completion: { (finished) in
                    //                    self.constraintTableViewBottom.constant = 199
                    if form.formTitle != selectedForm[0].formTitle {
                        let delayTime = DispatchTime.now() + 0.3
                        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                            displayRows()
                        })
                    } else {
                        let delayTime = DispatchTime.now() + 0.1
                        DispatchQueue.main.asyncAfter(deadline: delayTime, execute: {
                            self.tableViewForms.isUserInteractionEnabled = true
                            self.tableViewForms.reloadData()
                        })
                    }
                })
            } else {
                displayRows()
            }
        }
    }
    
    extension HomeViewController : UITableViewDataSource {
        func numberOfSections(in tableView: UITableView) -> Int {
            return formList.count
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            let form = formList[section]
            return form.isSelected == true ? form.subForms.count : 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm", for: indexPath) as! FormsTableViewCell
            let form = formList[indexPath.section].subForms[indexPath.row]
            //            if indexPath.section == 0 {
            //                let newPatientForms = formList[0].subForms!.filter({ (form) -> Bool in
            //                    return (form.formTitle == kMedicalHistory || form.formTitle == kMedicalHistorySpan)
            //                }).filter({ (form) -> Bool in
            //                    return form.isSelected == true
            //                })
            //                cell.labelFormName.textColor = indexPath.row == 0 || indexPath.row == 1 || newPatientForms.count > 0 ? UIColor.white : UIColor.white.withAlphaComponent(0.5)
            //            } else {
            cell.labelFormName.textColor = UIColor.white
            //            }
            
            cell.labelFormName.text = form.formTitle
            cell.imageViewCheckMark.isHidden = !form.isSelected
            cell.indexPath = indexPath
            if cell.gestureRecognizers == nil {
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureDidSelectAction))
                cell.addGestureRecognizer(tapGesture)
            }
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        }
    }
#else
    
    extension HomeViewController : UITableViewDelegate {
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if indexPath.row == consentIndex {
                let subForms = formList[consentIndex].subForms
                for subFrom in subForms! {
                    subFrom.isSelected = false
                }
                let form = self.formList[consentIndex]
                form.isSelected = !form.isSelected
                var indexPaths : [IndexPath] = [IndexPath]()
                for (idx, _) in form.subForms.enumerated() {
                    let indexPath = IndexPath(row: consentIndex + 1 + idx, section: 0)
                    indexPaths.append(indexPath)
                }
                if form.isSelected == true {
                    tableView.beginUpdates()
                    tableView.insertRows(at: indexPaths, with: .bottom)
                    tableView.endUpdates()
                    let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        if form.isSelected == true && indexPaths.count > 0 {
                            tableView.scrollToRow(at: IndexPath(row: tableView.numberOfRows(inSection: 0) - 1, section: 0), at: .bottom, animated: true)
                        }
                    }
                } else {
                    tableView.beginUpdates()
                    tableView.deleteRows(at: indexPaths, with: .bottom)
                    tableView.endUpdates()
                }
                tableView.reloadRows(at: [indexPath], with: .none)
                return
            }
            
            var form : Forms!
            if (indexPath.row <= consentIndex) {
                form = formList[indexPath.row]
            } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
                form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
            } else {
                form = formList.last
            }
            
            if !form.isSelected && form.formTitle == kNewPatientSignInForm {
                form.isSelected = true
                self.tableViewForms.reloadData()
            } else {
                form.isSelected = !form.isSelected
                tableView.reloadData()
            }
            if form.isSelected == true && form.isToothNumberRequired == true{
                PopupTextField.popUpView().showInViewController(self, WithTitle: "ENTER TOOTH NUMBERS *", placeHolder: "01,02,03,04,05", textFormat: TextFormat.ToothNumber, completion: { (popup, textField) in
                    if textField.isEmpty{
                        form.isSelected = false
                        tableView.reloadData()
                        
                    }else{
                        form.toothNumbers = textField.text!
                    }
                    popup.close()
                })
            }
            
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            let cell = tableView.dequeueReusableCell(withIdentifier: (indexPath.row <= consentIndex) ? "cellMainForm" : (formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count) ? "cellSubForm" : "cellMainForm") as! FormsTableViewCell
            var form : Forms!
            if (indexPath.row <= consentIndex) {
                form = formList[indexPath.row]
                let height = form.formTitle.heightWithConstrainedWidth(560, font: cell.labelFormName.font) + 24
                return height
            } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
                form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
                let height = form.formTitle.heightWithConstrainedWidth(520, font: cell.labelFormName.font) + 24
                return height
            } else {
                form = formList.last!
                let height = form.formTitle.heightWithConstrainedWidth(560, font: cell.labelFormName.font) + 24
                return height
            }
        }
    }
    
    extension HomeViewController : UITableViewDataSource {
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if formList.count > 0 {
                if formList.count > consentIndex {
                    return formList[consentIndex].isSelected == true ? formList.count + formList[consentIndex].subForms.count : formList.count
                } else {
                    return formList.count
                }
                
                //return formList.count
            } else {
                return 0
            }
            
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if (indexPath.row <= consentIndex) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellMainForm", for: indexPath) as! FormsTableViewCell
                let form = formList[indexPath.row]
                cell.labelFormName.text = form.formTitle
                cell.imageViewCheckMark.isHidden = !form.isSelected
                cell.backgroundColor = UIColor.clear
                cell.contentView.backgroundColor = UIColor.clear
                return cell
            } else if formList[consentIndex].isSelected && indexPath.row <= consentIndex + formList[consentIndex].subForms.count {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm", for: indexPath) as!FormsTableViewCell
                let form = formList[consentIndex].subForms[indexPath.row - (consentIndex + 1)]
                cell.labelFormName.text = form.formTitle
                cell.imageViewCheckMark.isHidden = !form.isSelected
                cell.backgroundColor = UIColor.clear
                cell.contentView.backgroundColor = UIColor.clear
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellMainForm", for: indexPath) as! FormsTableViewCell
                let form = formList[consentIndex + 1]
                cell.labelFormName.text = form.formTitle
                cell.imageViewCheckMark.isHidden = !form.isSelected
                cell.backgroundColor = UIColor.clear
                cell.contentView.backgroundColor = UIColor.clear
                return cell
            }
        }
        
    }
    
#endif

