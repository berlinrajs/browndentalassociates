//
//  CardImagePickerControllerNew.swift
//  ABCES
//
//  Created by Berlin Raj on 10/10/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
import AVFoundation

protocol CardImageCaptureDelegate {
    func cardImagePicker(_ picker: CardImagePickerControllerNew, completedWithCardImage image: UIImage?)
    func cardImagePickerDidCancel(_ picker: CardImagePickerControllerNew)
}

class CardImagePickerControllerNew: UIViewController {
    
    var captureSession = AVCaptureSession()
    var stillImageOutput: AVCaptureStillImageOutput!
    
    @IBOutlet var previewView: UIView!
    var delegate: CardImageCaptureDelegate!
    
    var isFrontCameraActive: Bool = false
    var capturePressed: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        edgesForExtendedLayout = UIRectEdge()
        captureSession.beginConfiguration()
        captureSession.sessionPreset = AVCaptureSessionPreset640x480
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.frame = CGRect(x: 130, y: 292, width: 505, height: 326)
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.previewView.layer.addSublayer(previewLayer!)
        
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        isFrontCameraActive = device?.position == AVCaptureDevicePosition.front ? true : false
        do {
            let input = try AVCaptureDeviceInput(device: device)
            captureSession.addInput(input)
            
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            captureSession.addOutput(stillImageOutput)
        } catch {
            
        }
        
        captureSession.commitConfiguration()
        captureSession.startRunning()
    }
    @IBAction func toogleDevice() {
        if capturePressed == true {
            capturePressed = false
            return
        }
        captureSession.stopRunning()
        captureSession.beginConfiguration()
        
        for input in captureSession.inputs {
            captureSession.removeInput(input as! AVCaptureInput)
        }
        
        for device in AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) {
            if ((device as AnyObject).position == AVCaptureDevicePosition.front && isFrontCameraActive == false) || ((device as AnyObject).position == AVCaptureDevicePosition.back && isFrontCameraActive == true) {
                do {
                    let input = try AVCaptureDeviceInput(device: device as! AVCaptureDevice)
                    captureSession.addInput(input)
                } catch {
                    
                }
            }
        }
        self.isFrontCameraActive = !isFrontCameraActive
        captureSession.commitConfiguration()
        captureSession.startRunning()
    }
    
    @IBAction func captureImage() {
        capturePressed = true
        var videoConnection: AVCaptureConnection!
        
        for connection in stillImageOutput.connections as! [AVCaptureConnection] {
            for port in connection.inputPorts {
                if (port as AnyObject).mediaType == AVMediaTypeVideo {
                    videoConnection = connection
                    break;
                }
            }
            if videoConnection != nil
            {
                break;
            }
        }
        print("about to request a capture from: \(stillImageOutput)")
        stillImageOutput.captureStillImageAsynchronously(from: videoConnection) { (buffer, error) in
            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
            let image = UIImage(data: imageData!)
            
            self.delegate.cardImagePicker(self, completedWithCardImage: image)
            self.capturePressed = false
        }
    }
    
    @IBAction func backAction() {
        if capturePressed == true {
            capturePressed = false
            return
        }
        captureSession.stopRunning()
        delegate.cardImagePickerDidCancel(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
