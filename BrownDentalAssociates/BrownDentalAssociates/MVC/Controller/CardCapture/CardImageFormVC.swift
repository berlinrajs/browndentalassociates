//
//  CardImageFormVC.swift
//  AceDental
//
//  Created by SRS Web Solutions on 29/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CardImageFormVC: MCViewController {

    @IBOutlet weak var imageViewFront: ActionImageView!
    @IBOutlet weak var imageViewBack: ActionImageView!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelBack: UILabel!
    
    var frontImage: UIImage!
    var backImage: UIImage?
    
    var isDrivingLicense: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()

        imageViewFront.borderColor = UIColor.lightGray
        imageViewBack.borderColor = UIColor.lightGray
        
        labelTitle.text = isDrivingLicense ? "DRIVING LICENSE" : "INSURANCE CARD"
        labelDate.text = "DATE: " + patient.dateToday
        
        labelName.text = "PATIENT NAME: " + patient.fullName.uppercased()
        
        self.imageViewFront.image = frontImage
        if backImage == nil {
            self.imageViewBack.isHidden = true
            self.labelBack.isHidden = true
        } else {
            self.imageViewBack.image = backImage
            self.imageViewBack.isHidden = false
            self.labelBack.isHidden = false
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  }
