//
//  OralScreeningViewController.swift
//  TotalHealthDental
//
//  Created by SRS Web Solutions on 30/03/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class OralScreeningViewController: MCViewController {

    var preferVelScope: Bool = true
    
//    @IBOutlet weak var buttonBack: PDButton!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonNo: UIButton!
    @IBOutlet weak var textViewFeeDetails: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName.text = patient.fullName
        labelDate.todayDate = patient.dateToday
        signatureView.layer.cornerRadius = 3.0
//        let tapGesture = UITapGestureRecognizer(target: self, action: Selector("setDateOnLabel"))
//        tapGesture.numberOfTapsRequired = 1
//        labelDate.addGestureRecognizer(tapGesture)
        
//        textViewFeeDetails.attributedText = self.detailAttributedString
        // Do any additional setup after loading the view.
    }
    var detailAttributedString: NSMutableAttributedString {
        get{
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Complete each time the examination is performed and place in the patient’s file\n\nOur practice continually looks for advances to ensure that we are providing the optimum level of oral health care to our patients. We are concerned about oral cancer and look for it in every patient.\n\nOne American dies every hour from oral cancer. Late detection of oral cancer is the primary cause that both the incidence and mortality rates of oral cancer continue to increase. As with most cancers, age is the primary risk factor for oral cancer. Tobacco and alcohol use are other major predisposing risk factors but more than 25% of oral cancer victims have no such life style risk factors. Studies also suggest that human papillomavirus (HPV) plays a roll in more than 20% of oral cancer causes. * Oral cancer risk by patient profile as follows:\n\nIncreased risk: patients ages 18-39 -sexually active patients (HPV)\nHigh risk: patients age 40 and older; tobacco users (ages 18-39, any type within 10 years)\nHighest risk: patients age 40 and older with lifestyle risk factors (tobacco and/or alcohol use); previous history of oral cancer\n\nWe have recently incorporated VELscope powered by Sapphire into our oral screening standard of care. We find that using VELscope powered by Sapphire along with a standard oral cancer examination improves the ability to identify suspicious areas at their earliest stages. VELscope Powered by Sapphire, along with the doctor’s visual exam, is similar to proven early detection procedures for other cancers such as mammography, Pap smear, and PSA. VELscope powered by Sapphire is a simple and painless examination that gives the best chance to find any abnormalities at the earliest possible stage. Early detection of pre-cancerous tissue can minimize or eliminate the potentially disfiguring effects of oral cancer and possibly save your life. The VELscope powered by Sapphire exam will be offered to you annually.\n\nThis enhanced examination is recognized by the American Dental Association code revision committee as CDT-2007/08 procedure code D0431; however, this exam might not be covered by your insurance. The fee for this enhanced examination is $20.")
            
//            let form = self.patient.selectedForms.last
//            let stringDetail = form!.additionalDetail!
//            let attr1: NSMutableAttributedString = NSMutableAttributedString(string: "   $ 20.00   .")
//            attr1.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: NSString(string: attr1.string).rangeOfString("   $ 20.00   "))
//            attributedString.appendAttributedString(attr1)
            
            attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.white, range: NSString(string: attributedString.string).range(of: attributedString.string))
            attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "WorkSans-Regular", size: 16)!, range: NSString(string: attributedString.string).range(of: attributedString.string))
            
            return attributedString
        }
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.black
    }
    
    @IBAction func changevelscopePreference()
    {
        if(self.buttonYes.isSelected)
        {
            self.buttonYes.isSelected = false
            self.buttonNo.isSelected = true
        }
        else
        {
            self.buttonYes.isSelected = true
            self.buttonNo.isSelected = false
        }
        self.preferVelScope = self.buttonYes.isSelected
    }
    
    @IBAction func buttonActionSubmit(sender: AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        } else {
            patient.signature1 = signatureView.signatureImage()
            let oralScreeningForm = consentStoryBoard.instantiateViewController(withIdentifier: "kOralScreeningFormVC") as! OralScreeningFormViewController
            oralScreeningForm.patient = patient
            oralScreeningForm.preferVelScope = self.preferVelScope
            self.navigationController?.pushViewController(oralScreeningForm, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
