//
//  OralScreeningFormViewController.swift
//  TotalHealthDental
//
//  Created by SRS Web Solutions on 30/03/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class OralScreeningFormViewController: MCViewController {

    var preferVelScope: Bool = true
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageViewSignature: UIImageView!
//    @IBOutlet weak var buttonSubmit: PDButton!
//    @IBOutlet weak var buttonBack: PDButton!
//    @IBOutlet weak var labelFeeDetails: UILabel!
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonNo: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName.text = patient.fullName
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        
        self.buttonYes.isSelected = self.preferVelScope
        self.buttonNo.isSelected = !self.preferVelScope
        
       // labelFeeDetails.attributedText = self.detailAttributedString
        
        // Do any additional setup after loading the view.
    }
    var detailAttributedString: NSMutableAttributedString {
        get{
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "This enhanced examination is recognized by the American Dental Association code revision committee as CDT-2007/08 procedure code D0431; however, this exam might not be covered by your insurance. The fee for this enhanced examination is $20.")
            
//            let form = self.patient.selectedForms.last
//            let stringDetail = form!.additionalDetail!
//            let attr1: NSMutableAttributedString = NSMutableAttributedString(string: "   $ 20.00   .")
//            attr1.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: NSString(string: attr1.string).rangeOfString("   $ 20.00   "))
//            attributedString.appendAttributedString(attr1)
            
            attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSString(string: attributedString.string).range(of: attributedString.string))
            attributedString.addAttribute(NSFontAttributeName, value: UIFont(name: "WorkSans-Regular", size: 12)!, range: NSString(string: attributedString.string).range(of: attributedString.string))
            
            return attributedString
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    @IBAction func buttonSubmitAction(sender: AnyObject) {
//        if !Reachability.isConnectedToNetwork() {
//            let alertController = UIAlertController(title: "Total Health Dental", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
//            let alertOkAction = UIAlertAction(title: "SETTINGS", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
//                if let url = settingsUrl {
//                    UIApplication.sharedApplication().openURL(url)
//                }
//            }
//            let alertCancelAction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                
//            }
//            alertController.addAction(alertOkAction)
//            alertController.addAction(alertCancelAction)
//            self.presentViewController(alertController, animated: true, completion: nil)
//        }
//        else {
//            let pdfManager = PDFManager()
//            pdfManager.authorizeDrive(self.view) { (success) -> Void in
//                if success {
//                    self.buttonSubmit.hidden = true
//                    self.buttonBack.hidden = true
//                    pdfManager.createPDFForView(self.view, fileName: "ORAL_SCREENING_FORM", patient: self.patient, completionBlock: { (finished) -> Void in
//                        if finished {
//                            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
//                            self.navigationController?.popToRootViewControllerAnimated(true)
//                        } else {
//                            self.buttonSubmit.hidden = false
//                            self.buttonBack.hidden = false
//                        }
//                    })
//                } else {
//                    self.buttonSubmit.hidden = false
//                    self.buttonBack.hidden = false
//                }
//            }
//        }
//    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
