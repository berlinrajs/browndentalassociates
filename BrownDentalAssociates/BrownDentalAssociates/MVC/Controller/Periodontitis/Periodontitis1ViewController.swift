//
//  Periodontitis1ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class Periodontitis1ViewController: MCViewController {

    @IBOutlet     var buttonTreatment : [UIButton]!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
        
        for btn in buttonTreatment{
            btn.isSelected = patient.periodontitisTag.contains(btn.tag)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onTreatmentButtonPressed (withSender sender : UIButton){
        sender.isSelected = !sender.isSelected
        if patient.periodontitisTag.contains(sender.tag){
            patient.periodontitisTag.remove(at: patient.periodontitisTag.index(of: sender.tag)!)
        }else{
            patient.periodontitisTag.append(sender.tag)
        }
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if patient.periodontitisTag.count == 0{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let periodontitis = consentStoryBoard.instantiateViewController(withIdentifier: "PeriodontitisFormVC") as! PeriodontitisFormViewController
            periodontitis.patient = self.patient
            periodontitis.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(periodontitis, animated: true)

        }
    }
    



}
