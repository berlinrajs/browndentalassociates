//
//  PeriodontitisFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class PeriodontitisFormViewController: MCViewController {

    var signPatient : UIImage!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet      var buttonTreatment : [UIButton]!

    override func viewDidLoad() {
        super.viewDidLoad()

        labelPatientName.text = patient.fullName
        labelDate1.text = patient.dateToday
        signaturePatient.image = signPatient
        for btn in buttonTreatment{
            btn.isSelected = patient.periodontitisTag.contains(btn.tag)
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
