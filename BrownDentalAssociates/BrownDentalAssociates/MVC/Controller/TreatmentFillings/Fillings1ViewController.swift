//
//  Fillings1ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class Fillings1ViewController: MCViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var signatureWitness : SignatureView!
    @IBOutlet weak var signatureDoctor : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    @IBOutlet weak var labelDate2 : DateLabel!
    @IBOutlet weak var labelDate3 : DateLabel!
    @IBOutlet      var initialSignatureViews : [SignatureView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        labelDate3.todayDate = patient.dateToday

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() || !signatureWitness.isSigned() || !signatureDoctor.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped || !labelDate2.dateTapped || !labelDate3.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let filling = consentStoryBoard.instantiateViewController(withIdentifier: "FillingsFormVC") as! FillingsFormViewController
            filling.patient = self.patient
            filling.signPatient = signaturePatient.signatureImage()
            filling.signWitness = signatureWitness.signatureImage()
            filling.signDoctor = signatureDoctor.signatureImage()
            for imgvw in initialSignatureViews{
                let image = imgvw.signatureImage() == nil ? UIImage() : imgvw.signatureImage()
                filling.signInitials.append(image!)
            }
            self.navigationController?.pushViewController(filling, animated: true)
            
        }
    }



}
