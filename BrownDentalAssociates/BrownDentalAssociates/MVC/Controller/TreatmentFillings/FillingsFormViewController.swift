//
//  FillingsFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/19/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class FillingsFormViewController: MCViewController {

    var signPatient : UIImage!
    var signWitness : UIImage!
    var signDoctor : UIImage!
    var signInitials : [UIImage] = [UIImage]()
    @IBOutlet      var initialImageView : [UIImageView]!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDOB : UILabel!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelDate3 : UILabel!
    @IBOutlet weak var signaturePatient  : UIImageView!
    @IBOutlet weak var signatureWitness : UIImageView!
    @IBOutlet weak var signatureDoctor : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        for img in initialImageView{
            img.image = signInitials[img.tag]
        }
        labelPatientName.text = patient.fullName
        labelDOB.text = patient.dateOfBirth
        labelDate.text = patient.dateToday
        labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelDate3.text = patient.dateToday
        signaturePatient.image = signPatient
        signatureWitness.image = signWitness
        signatureDoctor.image = signDoctor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
