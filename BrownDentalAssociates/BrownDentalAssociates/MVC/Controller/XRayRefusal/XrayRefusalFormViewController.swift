//
//  XrayRefusalFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/16/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class XrayRefusalFormViewController: MCViewController {

    var signPatient : UIImage!
    @IBOutlet weak var labelLastName : UILabel!
    @IBOutlet weak var labelFirstName : UILabel!
    @IBOutlet weak var labelInitial : UILabel!
    @IBOutlet weak var labelPreferredName : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate : UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        labelLastName.text = patient.lastName
        labelFirstName.text = patient.firstName
        labelInitial.text = patient.initial
        labelPreferredName.text = patient.preferredName
        signaturePatient.image = signPatient
        labelDate.text = patient.dateToday
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
