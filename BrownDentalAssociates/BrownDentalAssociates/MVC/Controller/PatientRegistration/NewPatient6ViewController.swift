//
//  NewPatient6ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/20/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient6ViewController: MCViewController {

    var isPrimaryInsurance : Bool!
    @IBOutlet weak var textfieldInsuredName : MCTextField!
    @IBOutlet weak var textfieldSocialSecurity : MCTextField!
    @IBOutlet weak var textfieldBirthDate : MCTextField!
    @IBOutlet weak var textfieldEmployer : MCTextField!
    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textfieldCity :MCTextField!
    @IBOutlet weak var textfieldState : MCTextField!
    @IBOutlet weak var textfieldZipcode : MCTextField!
    @IBOutlet weak var dropDownRelation : BRDropDown!
    @IBOutlet weak var labelTitle : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldSocialSecurity.textFormat = .SocialSecurity
        textfieldBirthDate.textFormat = .DateIn1980
        textfieldState.textFormat = .State
        textfieldZipcode.textFormat = .Zipcode
        labelTitle.text = isPrimaryInsurance == true ? "PRIMARY INSURANCE" : "SECONDARY INSURANCE"
        dropDownRelation.items = ["SELF","SPOUSE","CHILD","OTHER"]
        dropDownRelation.placeholder = "-- RELATIONSHIP TO PATIENT --"
        dropDownRelation.delegate = self
    
         loadValue()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValue (){
        let ins : Insurance = isPrimaryInsurance == true ? patient.primaryInsurance : patient.secondaryInsurance
        
        textfieldSocialSecurity.setSavedText(text: ins.socialSecurity)
        textfieldBirthDate.setSavedText(text: ins.dateOfBirth)
        textfieldEmployer.setSavedText(text: ins.employerName)
        textfieldAddress.setSavedText(text: ins.employerAddress)
        textfieldCity.setSavedText(text: ins.employerCity)
        textfieldState.setSavedText(text: ins.employerState)
        textfieldZipcode.setSavedText(text: ins.employerZipcode)
        dropDownRelation.selectedIndex = ins.relationTag
        
        func loadVal(){
        textfieldInsuredName.setSavedText(text: ins.nameInsured)
            
        }
        
        #if AUTO
            if let patientDetails = patient.patientDetails {
                if let insuranceAuto = isPrimaryInsurance == true ? patientDetails.primaryDentalInsurance : patientDetails.secondaryDentalInsurance {
                    textfieldInsuredName.setSavedText(text: ins.nameInsured != nil ? ins.nameInsured :  insuranceAuto.name)
                    dropDownRelation.selectedIndex = insuranceAuto.relation == nil ? 0 : insuranceAuto.relation!.index
                } else {
                    loadVal()
                }
            } else {
                loadVal()
            }
        #else
            loadVal()
        #endif
        
    }
    
    func saveValue (){
        let ins : Insurance = isPrimaryInsurance == true ? patient.primaryInsurance : patient.secondaryInsurance
        ins.nameInsured = textfieldInsuredName.getText()
        ins.socialSecurity = textfieldSocialSecurity.getText()
        ins.dateOfBirth = textfieldBirthDate.getText()
        ins.employerName = textfieldEmployer.getText()
        ins.employerAddress = textfieldAddress.getText()
        ins.employerCity = textfieldCity.getText()
        ins.employerState = textfieldState.getText()
        ins.employerZipcode = textfieldZipcode.getText()
        ins.relationTag = dropDownRelation.selectedOption == nil ? 0 : dropDownRelation.selectedIndex
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldZipcode.isEmpty && !textfieldZipcode.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else if !textfieldSocialSecurity.isEmpty && !textfieldSocialSecurity.text!.isSocialSecurityNumber{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else{
            saveValue()
            let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient7VC") as! NewPatient7ViewController
            patient.isPrimaryInsurance = isPrimaryInsurance
            patient.patient = self.patient
            self.navigationController?.pushViewController(patient, animated: true)
        }
        
    }
}

extension NewPatient6ViewController : BRDropDownDelegate{
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if index == 1{
            textfieldInsuredName.setSavedText(text: patient.fullName)
            textfieldBirthDate.setSavedText(text: patient.dateOfBirth)
            textfieldSocialSecurity.setSavedText(text: patient.socialSecurity)
        }else{
            textfieldInsuredName.text = ""
            textfieldBirthDate.text = ""
            textfieldSocialSecurity.text = ""
        }
        let ins : Insurance = isPrimaryInsurance == true ? patient.primaryInsurance : patient.secondaryInsurance

        if index == 4{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "", placeHolder: "PLEASE SPECIFY", textFormat: TextFormat.Default, completion: { (popUp, textField) in
                if textField.isEmpty{
                    self.dropDownRelation.reset()
                    ins.relationOther = "N/A"
                }else{
                    ins.relationOther = textField.text!
                }
                popUp.close()
            })
        }else{
            ins.relationOther = "N/A"

        }
    }
}
