//
//  NewPatient4ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/20/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient4ViewController: MCViewController {

    @IBOutlet weak var textfieldEmail : MCTextField!
    @IBOutlet weak var textfieldHomePhone : MCTextField!
    @IBOutlet weak var textfieldWorkPhone : MCTextField!
    @IBOutlet weak var textfieldCellPhone : MCTextField!
    @IBOutlet weak var textfieldSSN : MCTextField!
    @IBOutlet weak var textfieldDriverLicense : MCTextField!
    @IBOutlet weak var dropDownMaritialStatus : BRDropDown!
    @IBOutlet weak var textfieldDOB : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldEmail.textFormat = .Email
        textfieldHomePhone.textFormat = .Phone
        textfieldWorkPhone.textFormat = .Phone
        textfieldCellPhone.textFormat = .Phone
        textfieldSSN.textFormat = .SocialSecurity
        textfieldDriverLicense.textFormat = .AlphaNumeric
        textfieldDOB.textFormat = .DateIn1980
        dropDownMaritialStatus.items = ["MARRIED","SINGLE","DIVORCED","WIDOWED"]
        dropDownMaritialStatus.placeholder = "-- SELECT MARITAL STATUS --"
        loadValue()


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldEmail.setSavedText(text: patient.responsibleEmail)
        textfieldHomePhone.setSavedText(text: patient.responsibleHomePhone)
        textfieldWorkPhone.setSavedText(text: patient.responsibleWorkPhone)
        textfieldCellPhone.setSavedText(text: patient.responsibleCellPhone)
        textfieldSSN.setSavedText(text: patient.responsibleSocialSecurity)
        textfieldDriverLicense.setSavedText(text: patient.responsibleDriverLicense)
        textfieldDOB.setSavedText(text: patient.responsibleBirthdate)
        dropDownMaritialStatus.selectedIndex = patient.responsibleMaritalStatus

    }
    
    func saveValue (){
        patient.responsibleEmail = textfieldEmail.getText()
        patient.responsibleHomePhone = textfieldHomePhone.getText()
        patient.responsibleWorkPhone = textfieldWorkPhone.getText()
        patient.responsibleCellPhone = textfieldCellPhone.getText()
        patient.responsibleSocialSecurity = textfieldSSN.getText()
        patient.responsibleDriverLicense = textfieldDriverLicense.getText()
        patient.responsibleBirthdate = textfieldDOB.getText()
        patient.responsibleMaritalStatus = dropDownMaritialStatus.selectedOption == nil ? 0 : dropDownMaritialStatus.selectedIndex
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }

    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldEmail.isEmpty && !textfieldEmail.text!.isValidEmail{
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        }else if !textfieldHomePhone.isEmpty && !textfieldHomePhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID HOME PHONE NUMBER")
        }else if !textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else if !textfieldCellPhone.isEmpty && !textfieldCellPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID CELL PHONE NUMBER")
        }else if !textfieldSSN.isEmpty && !textfieldSSN.text!.isSocialSecurityNumber{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else{
            saveValue()
            if self.patient.responsibleMaritalStatus == 1{
                let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient5VC") as! NewPatient5ViewController
                patient.patient = self.patient
                self.navigationController?.pushViewController(patient, animated: true)
            }else{
                CustomAlert.yesOrNoAlertView().showWithQuestion("DO YOU HAVE INSURANCE", completion: { (index) in
                    if index == 1{
                        let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient6VC") as! NewPatient6ViewController
                        patient.isPrimaryInsurance = true
                        patient.patient = self.patient
                        self.navigationController?.pushViewController(patient, animated: true)
                    }else{
                        let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient8VC") as! NewPatient8ViewController
                        patient.patient = self.patient
                        self.navigationController?.pushViewController(patient, animated: true)
                    }
                })
            }
        }
        
    }


}
