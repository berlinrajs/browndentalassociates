//
//  NewPatientFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/21/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatientFormViewController: MCViewController {

    
    @IBOutlet weak var labelName1 : UILabel!
    @IBOutlet weak var labelRelationship1 : UILabel!
    @IBOutlet weak var labelIdentifier1 : UILabel!
    @IBOutlet weak var labelName2 : UILabel!
    @IBOutlet weak var labelRelationship2 : UILabel!
    @IBOutlet weak var labelIdentifier2 : UILabel!
    @IBOutlet      var imageViewInitials : [UIImageView]!
    @IBOutlet weak var imageviewPatient : UIImageView!
    @IBOutlet weak var labelDate : UILabel!

    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelFirstName : UILabel!
    @IBOutlet weak var labelLastName : UILabel!
    @IBOutlet weak var labelAddress : UILabel!
    @IBOutlet weak var labelCity : UILabel!
    @IBOutlet weak var labelEmail : UILabel!
    @IBOutlet weak var labelHomePhone : UILabel!
    @IBOutlet weak var labelWorkPhone : UILabel!
    @IBOutlet weak var labelCellPhone : UILabel!
    @IBOutlet weak var labelBirthdate : UILabel!
    @IBOutlet weak var labelSocialSecurity : UILabel!
    @IBOutlet weak var labelDriverLicense : UILabel!
    @IBOutlet weak var radioGender : RadioButton!
    
    @IBOutlet weak var labelResponsibleFirstName  : UILabel!
    @IBOutlet weak var labelResponsibleInitial : UILabel!
    @IBOutlet weak var labelResponsibleLastName : UILabel!
    @IBOutlet weak var labelResponsibleAddress : UILabel!
    @IBOutlet weak var labelResponsibleCity : UILabel!
    @IBOutlet weak var radioResponsibleGender : RadioButton!
    @IBOutlet weak var labelResponsibleEmail : UILabel!
    @IBOutlet weak var labelResponsibleHomephone : UILabel!
    @IBOutlet weak var labelResponsibleWorkPhone : UILabel!
    @IBOutlet weak var labelResponsibleCellPhone : UILabel!
    @IBOutlet weak var labelResponsibleBirthdate : UILabel!
    @IBOutlet weak var labelResponsibleSocialSecurity : UILabel!
    @IBOutlet weak var labelResponsibleDriverLicense : UILabel!
    @IBOutlet weak var radioMaritalStatus : RadioButton!
    @IBOutlet weak var labelSpouseName : UILabel!
    @IBOutlet weak var labelSpouseHomePhone : UILabel!
    @IBOutlet weak var labelSpouseWorkPhone : UILabel!
    @IBOutlet weak var labelSpouseCellPhone : UILabel!
    @IBOutlet weak var labelSpouseBirthdate : UILabel!
    @IBOutlet weak var labelSpouseSocialSecurity : UILabel!
    @IBOutlet weak var labelSpouseDriverLicense : UILabel!
    
    @IBOutlet weak var labelInsuranceName : UILabel!
    @IBOutlet weak var labelInsuranceSocialSecurity : UILabel!
    @IBOutlet weak var labelInsuranceBirthdate : UILabel!
    @IBOutlet weak var radioInsuranceRelationship : RadioButton!
    @IBOutlet weak var labelInsuranceRelationOther : UILabel!
    @IBOutlet weak var labelInsuranceEmployerName : UILabel!
    @IBOutlet weak var labelInsuranceEmployerAddress : UILabel!
    @IBOutlet weak var labelInsuranceEmployerCity : UILabel!
    @IBOutlet weak var labelInsuranceCompanyName : UILabel!
    @IBOutlet weak var labelInsuranceCompanyAddress : UILabel!
    @IBOutlet weak var labelInsuranceCompanyCity : UILabel!
    @IBOutlet weak var labelGroup : UILabel!
    @IBOutlet weak var labelMemberId : UILabel!
    
    @IBOutlet weak var labelInsuranceName1 : UILabel!
    @IBOutlet weak var labelInsuranceSocialSecurity1 : UILabel!
    @IBOutlet weak var labelInsuranceBirthdate1 : UILabel!
    @IBOutlet weak var radioInsuranceRelationship1 : RadioButton!
    @IBOutlet weak var labelInsuranceRelationOther1 : UILabel!
    @IBOutlet weak var labelInsuranceEmployerName1 : UILabel!
    @IBOutlet weak var labelInsuranceEmployerAddress1 : UILabel!
    @IBOutlet weak var labelInsuranceEmployerCity1 : UILabel!
    @IBOutlet weak var labelInsuranceCompanyName1 : UILabel!
    @IBOutlet weak var labelInsuranceCompanyAddress1 : UILabel!
    @IBOutlet weak var labelInsuranceCompanyCity1 : UILabel!
    @IBOutlet weak var labelGroup1 : UILabel!
    @IBOutlet weak var labelMemberId1 : UILabel!
    
    @IBOutlet weak var labelEmergencyName : UILabel!
    @IBOutlet weak var labelEmergencyPhone : UILabel!
    @IBOutlet weak var labelEmergencyRelation : UILabel!
    @IBOutlet weak var labelChooseBrownDental : UILabel!
    @IBOutlet weak var labelReference : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loadValue()  {
        labelName1.text = patient.name1
        labelRelationship1.text = patient.relationship1
        labelIdentifier1.text = patient.identifier1
        labelName2.text = patient.name2
        labelRelationship2.text = patient.relationship2
        labelIdentifier2.text = patient.identifier2
        for (idx,imgv) in imageViewInitials.enumerated(){
            imgv.image = patient.signatureInitials[idx]
        }
        imageviewPatient.image = patient.signPatient
        labelDate.text = patient.dateToday
        
        labelDate1.text = patient.dateToday
        labelFirstName.text = patient.firstName
        labelLastName.text = patient.lastName
        labelAddress.text = patient.address
        labelCity.text = patient.city + ", " +  patient.state + ", " +  patient.zipcode
        labelEmail.text = patient.email
        labelHomePhone.text = patient.homePhone
        labelWorkPhone.text = patient.workPhone
        labelCellPhone.text = patient.cellPhone
        labelBirthdate.text = patient.dateOfBirth
        labelSocialSecurity.text = patient.socialSecurity.socialSecurityNumber
        labelDriverLicense.text = patient.driverLicense
        radioGender.setSelectedWithTag(patient.genderTag)
        
        labelResponsibleFirstName.text = patient.responsibleFirstName
        labelResponsibleInitial.text = patient.responsibleInitial
        labelResponsibleLastName.text = patient.responsibleLastName
        labelResponsibleAddress.text = patient.responsibleAddress
        labelResponsibleCity.text = formatAddress(strArray: [patient.responsibleCity,patient.responsibleState,patient.responsibleZipcode])
        radioResponsibleGender.setSelectedWithTag(patient.responsibleGenderTag)
        labelResponsibleEmail.text = patient.responsibleEmail
        labelResponsibleHomephone.text = patient.responsibleHomePhone
        labelResponsibleWorkPhone.text = patient.responsibleWorkPhone
        labelResponsibleCellPhone.text = patient.responsibleCellPhone
        labelResponsibleBirthdate.text = patient.responsibleBirthdate
        labelResponsibleSocialSecurity.text = patient.responsibleSocialSecurity.socialSecurityNumber
        labelResponsibleDriverLicense.text = patient.responsibleDriverLicense
        radioMaritalStatus.setSelectedWithTag(patient.responsibleMaritalStatus)
        labelSpouseName.text = patient.spouseName
        labelSpouseHomePhone.text = patient.spouseHomePhone
        labelSpouseWorkPhone.text = patient.spouseWorkPhone
        labelSpouseCellPhone.text = patient.spouseCellPhone
        labelSpouseBirthdate.text = patient.spouseBirthdate
        if patient.spouseSocialSecurity != nil{
        labelSpouseSocialSecurity.text = patient.spouseSocialSecurity.socialSecurityNumber
        }
        labelSpouseDriverLicense.text = patient.spouseDriverLicense
        
        let ins : Insurance = patient.primaryInsurance
        labelInsuranceName.text = ins.nameInsured
        labelInsuranceSocialSecurity.text = ins.socialSecurity == nil ? "" : ins.socialSecurity.socialSecurityNumber
        labelInsuranceBirthdate.text = ins.dateOfBirth
        radioInsuranceRelationship.setSelectedWithTag(ins.relationTag)
        labelInsuranceRelationOther.text = ins.relationOther
        labelInsuranceEmployerName.text = ins.employerName
        labelInsuranceEmployerAddress.text = ins.employerAddress
        labelInsuranceEmployerCity.text = formatAddress(strArray: [ins.employerCity == nil ? "" : ins.employerCity,ins.employerState == nil ? "" : ins.employerState,ins.employerZipcode == nil ? "" : ins.employerZipcode]) == "N/A" ? "" : formatAddress(strArray: [ins.employerCity == nil ? "" : ins.employerCity,ins.employerState == nil ? "" : ins.employerState,ins.employerZipcode == nil ? "" : ins.employerZipcode])
        labelInsuranceCompanyName.text = ins.insuranceCompanyName
        labelInsuranceCompanyAddress.text = ins.insuranceCompanyAddress
        labelInsuranceCompanyCity.text = formatAddress(strArray: [ins.insuranceCompanyCity == nil ? "" : ins.insuranceCompanyCity,ins.insuranceCompanyState == nil ? "" : ins.insuranceCompanyState,ins.insuranceCompanyZipcode == nil ? "" : ins.insuranceCompanyZipcode]) == "N/A" ? "" : formatAddress(strArray: [ins.insuranceCompanyCity == nil ? "" : ins.insuranceCompanyCity,ins.insuranceCompanyState == nil ? "" : ins.insuranceCompanyState,ins.insuranceCompanyZipcode == nil ? "" : ins.insuranceCompanyZipcode])
        labelGroup.text = ins.groupNumber
        labelMemberId.text = ins.memberID
        
        let ins1 : Insurance = patient.secondaryInsurance
        labelInsuranceName1.text = ins1.nameInsured
        labelInsuranceSocialSecurity1.text = ins1.socialSecurity == nil ? "" : ins1.socialSecurity.socialSecurityNumber
        labelInsuranceBirthdate1.text = ins1.dateOfBirth
        radioInsuranceRelationship1.setSelectedWithTag(ins1.relationTag)
        labelInsuranceRelationOther1.text = ins1.relationOther
        labelInsuranceEmployerName1.text = ins1.employerName
        labelInsuranceEmployerAddress1.text = ins1.employerAddress
        labelInsuranceEmployerCity1.text = formatAddress(strArray: [ins1.employerCity == nil ? "" : ins1.employerCity,ins1.employerState == nil ? "" : ins1.employerState,ins1.employerZipcode == nil ? "" : ins1.employerZipcode]) == "N/A" ? "" : formatAddress(strArray: [ins1.employerCity == nil ? "" : ins1.employerCity,ins1.employerState == nil ? "" : ins1.employerState,ins1.employerZipcode == nil ? "" : ins1.employerZipcode])

        labelInsuranceCompanyName1.text = ins1.insuranceCompanyName
        labelInsuranceCompanyAddress1.text = ins1.insuranceCompanyAddress
        labelInsuranceCompanyCity1.text = formatAddress(strArray: [ins1.insuranceCompanyCity == nil ? "" : ins1.insuranceCompanyCity,ins1.insuranceCompanyState == nil ? "" : ins1.insuranceCompanyState,ins1.insuranceCompanyZipcode == nil ? "" : ins1.insuranceCompanyZipcode]) == "N/A" ? "" : formatAddress(strArray: [ins1.insuranceCompanyCity == nil ? "" : ins1.insuranceCompanyCity,ins1.insuranceCompanyState == nil ? "" : ins1.insuranceCompanyState,ins1.insuranceCompanyZipcode == nil ? "" : ins1.insuranceCompanyZipcode])
        labelGroup1.text = ins1.groupNumber
        labelMemberId1.text = ins1.memberID

        labelEmergencyName.text = patient.emergencyName
        labelEmergencyPhone.text = patient.emergencyPhone
        labelEmergencyRelation.text = patient.emergencyRelation
        labelChooseBrownDental.text = patient.chooseBrownDental
        labelReference.text = patient.reference
        
        
        #if AUTO
            var address1 = [String]()
            
            if patient.patientDetails!.city.characters.count > 0 {
                address1.append(patient.patientDetails!.city)
            }
            if patient.patientDetails!.state.characters.count > 0 {
                address1.append(patient.patientDetails!.state)
            }
            if patient.patientDetails!.zipCode.characters.count > 0 {
                address1.append(patient.patientDetails!.zipCode)
            }
            
            if let patientDetails = self.patient.patientDetails {
                labelAddress.compare(val1: address1.joined(separator: ", "), val2: address1.joined(separator: ", "))
                labelHomePhone.compare(val1: patient.homePhone, val2: patientDetails.homePhone.formattedPhoneNumber)
                labelCellPhone.compare(val1: patient.cellPhone, val2: patientDetails.cellPhone.formattedPhoneNumber)
                labelEmail.compare(val1: patient.email, val2: patientDetails.email)
                labelWorkPhone.compare(val1: patient.workPhone, val2: patientDetails.workPhone)
                labelSocialSecurity.compare(val1: patient.socialSecurity.socialSecurityNumber, val2: patientDetails.socialSecurityNumber.socialSecurityNumber)
                
                
                
                if let insurance = patientDetails.primaryDentalInsurance {
                    labelInsuranceName.compare(val1: insurance.name, val2: patient.primaryInsurance.nameInsured)
                    labelMemberId.compare(val1: insurance.insuredID, val2: patient.primaryInsurance.memberID)
                    labelGroup.compare(val1: insurance.groupNumber, val2: patient.primaryInsurance.groupNumber)
                }
                
                if let insurance = patientDetails.secondaryDentalInsurance {
                    labelInsuranceName1.compare(val1: insurance.name, val2: patient.secondaryInsurance.nameInsured)
                    labelMemberId1.compare(val1: insurance.insuredID, val2: patient.secondaryInsurance.memberID)
                    labelGroup1.compare(val1: insurance.groupNumber, val2: patient.secondaryInsurance.groupNumber)
                }
            }
        #endif

    }
    
    
    func formatAddress (strArray : [String]) -> String{
        var array : [String] = [String]()
        for str in strArray{
            if str != "N/A" && str != ""{
                array.append(str)
        }
    }
        return array.count == 0 ? "N/A" : (array as NSArray).componentsJoined(by: ", ")
    }
}
