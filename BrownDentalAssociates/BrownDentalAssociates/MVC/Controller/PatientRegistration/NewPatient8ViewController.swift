//
//  NewPatient8ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/21/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient8ViewController: MCViewController {

    @IBOutlet weak var textfieldName : MCTextField!
    @IBOutlet weak var textfieldPhone : MCTextField!
    @IBOutlet weak var textfieldRelation : MCTextField!
    @IBOutlet weak var textfieldChoose : MCTextField!
    @IBOutlet weak var textfieldReferral : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldPhone.textFormat = .Phone
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldName.setSavedText(text: patient.emergencyName)
        textfieldPhone.setSavedText(text: patient.emergencyPhone)
        textfieldRelation.setSavedText(text: patient.emergencyRelation)
        textfieldChoose.setSavedText(text: patient.chooseBrownDental)
        textfieldReferral.setSavedText(text: patient.reference)
        
    }
    
    func saveValue (){
        patient.emergencyName = textfieldName.getText()
        patient.emergencyPhone = textfieldPhone.getText()
        patient.emergencyRelation = textfieldRelation.getText()
        patient.chooseBrownDental = textfieldChoose.getText()
        patient.reference = textfieldReferral.getText()
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldPhone.isEmpty && !textfieldPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
        }else{
            saveValue()
            let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient9VC") as! NewPatient9ViewController
            patient.patient = self.patient
            self.navigationController?.pushViewController(patient, animated: true)
        }
        
    }
    


}
