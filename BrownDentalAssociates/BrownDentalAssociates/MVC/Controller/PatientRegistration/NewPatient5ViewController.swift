//
//  NewPatient5ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/20/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient5ViewController: MCViewController {

    @IBOutlet weak var textfieldSPouseName : MCTextField!
    @IBOutlet weak var textfieldHomePhone : MCTextField!
    @IBOutlet weak var textfieldWorkPhone : MCTextField!
    @IBOutlet weak var textfieldCellPhone : MCTextField!
    @IBOutlet weak var textfieldSSN : MCTextField!
    @IBOutlet weak var textfieldDriverLicense : MCTextField!
    @IBOutlet weak var textfieldDOB : MCTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        textfieldHomePhone.textFormat = .Phone
        textfieldWorkPhone.textFormat = .Phone
        textfieldCellPhone.textFormat = .Phone
        textfieldSSN.textFormat = .SocialSecurity
        textfieldDriverLicense.textFormat = .AlphaNumeric
        textfieldDOB.textFormat = .DateIn1980
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldSPouseName.setSavedText(text: patient.spouseName)
        textfieldHomePhone.setSavedText(text: patient.spouseHomePhone)
        textfieldWorkPhone.setSavedText(text: patient.spouseWorkPhone)
        textfieldCellPhone.setSavedText(text: patient.spouseCellPhone)
        textfieldSSN.setSavedText(text: patient.spouseSocialSecurity)
        textfieldDriverLicense.setSavedText(text: patient.spouseDriverLicense)
        textfieldDOB.setSavedText(text: patient.spouseBirthdate)
    }
    
    func saveValue (){
        patient.spouseName = textfieldSPouseName.getText()
        patient.spouseHomePhone = textfieldHomePhone.getText()
        patient.spouseWorkPhone = textfieldWorkPhone.getText()
        patient.spouseCellPhone = textfieldCellPhone.getText()
        patient.spouseSocialSecurity = textfieldSSN.getText()
        patient.spouseDriverLicense = textfieldDriverLicense.getText()
        patient.spouseBirthdate = textfieldDOB.getText()
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldHomePhone.isEmpty && !textfieldHomePhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID HOME PHONE NUMBER")
        }else if !textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else if !textfieldCellPhone.isEmpty && !textfieldCellPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID CELL PHONE NUMBER")
        }else if !textfieldSSN.isEmpty && !textfieldSSN.text!.isSocialSecurityNumber{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else{
            saveValue()
            CustomAlert.yesOrNoAlertView().showWithQuestion("DO YOU HAVE INSURANCE", completion: { (index) in
                if index == 1{
                    let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient6VC") as! NewPatient6ViewController
                    patient.isPrimaryInsurance = true
                    patient.patient = self.patient
                    self.navigationController?.pushViewController(patient, animated: true)

                }else{
                    let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient8VC") as! NewPatient8ViewController
                    patient.patient = self.patient
                    self.navigationController?.pushViewController(patient, animated: true)
                }
            })
        }
    }
}
