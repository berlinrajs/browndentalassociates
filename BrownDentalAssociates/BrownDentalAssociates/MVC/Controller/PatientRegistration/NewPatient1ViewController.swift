//
//  NewPatient1ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/20/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient1ViewController: MCViewController {

    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textfieldCity : MCTextField!
    @IBOutlet weak var textfieldState : MCTextField!
    @IBOutlet weak var textfieldZipcode : MCTextField!
    @IBOutlet weak var textfieldDriverLicense : MCTextField!
    @IBOutlet weak var dropdownGender : BRDropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldState.textFormat = .State
        textfieldZipcode.textFormat = .Zipcode
        textfieldDriverLicense.textFormat = .AlphaNumeric
        dropdownGender.items = ["MALE","FEMALE"]
        dropdownGender.placeholder = "-- SELECT GENDER * --"
        loadvalue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadvalue (){
        
        func loadMan(){
        textfieldAddress.setSavedText(text: patient.address)
        textfieldCity.setSavedText(text: patient.city)
        textfieldState.setSavedText(text: patient.state)
        textfieldZipcode.setSavedText(text: patient.zipcode)
        textfieldDriverLicense.setSavedText(text: patient.driverLicense)
        dropdownGender.selectedIndex = patient.genderTag
        }
        #if AUTO
            if let patientDetails = patient.patientDetails {
                textfieldAddress.setSavedText(text: patientDetails.address)
                textfieldState.text = patientDetails.state
                textfieldCity.setSavedText(text: patientDetails.city)
                textfieldZipcode.setSavedText(text: patientDetails.zipCode)
                dropdownGender.selectedIndex = patientDetails.gender
                
            } else {
                loadMan()
            }
        #else
            loadMan()
        #endif
    }
    
    func saveValue() {
        patient.address = textfieldAddress.getText()
        patient.city = textfieldCity.getText()
        patient.state = textfieldState.getText()
        patient.zipcode = textfieldZipcode.getText()
        patient.driverLicense = textfieldDriverLicense.getText()
        patient.genderTag = dropdownGender.selectedOption == nil ? 0 : dropdownGender.selectedIndex
    }

    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty || dropdownGender.selectedOption == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if !textfieldZipcode.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else{
            saveValue()
            let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient2VC") as! NewPatient2ViewController
            patient.patient = self.patient
            self.navigationController?.pushViewController(patient, animated: true)
        }
    }
}
