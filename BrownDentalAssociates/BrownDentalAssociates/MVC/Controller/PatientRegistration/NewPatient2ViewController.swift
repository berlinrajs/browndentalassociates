//
//  NewPatient2ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/20/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient2ViewController: MCViewController {

    @IBOutlet weak var textfieldEmail : MCTextField!
    @IBOutlet weak var textfieldHomePhone : MCTextField!
    @IBOutlet weak var textfieldWorkPhone : MCTextField!
    @IBOutlet weak var textfieldCellPhone : MCTextField!
    @IBOutlet weak var textfieldSSN : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textfieldEmail.textFormat = .Email
        textfieldHomePhone.textFormat = .Phone
        textfieldWorkPhone.textFormat = .Phone
        textfieldCellPhone.textFormat = .Phone
        textfieldSSN.textFormat = .SocialSecurity
        loadvalue()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadvalue (){
        
        func loadMan(){
        textfieldEmail.setSavedText(text: patient.email)
        textfieldHomePhone.setSavedText(text: patient.homePhone)
        textfieldWorkPhone.setSavedText(text: patient.workPhone)
        textfieldCellPhone.setSavedText(text: patient.cellPhone)
        textfieldSSN.setSavedText(text: patient.socialSecurity)
        }
        #if AUTO
            if let patientDetails = patient.patientDetails {
                textfieldEmail.setSavedText(text: patient.email != nil ? patient.email : patientDetails.email)
                textfieldHomePhone.setSavedText(text: patient.homePhone != nil ? patient.homePhone : patientDetails.homePhone)
                textfieldWorkPhone.setSavedText(text: patient.workPhone != nil ? patient.workPhone :  patientDetails.workPhone)
                textfieldCellPhone.setSavedText(text: patient.cellPhone != nil ? patient.cellPhone :  patientDetails.cellPhone)
                textfieldSSN.setSavedText(text: patient.socialSecurity != nil ? patient.socialSecurity :  patientDetails.socialSecurityNumber)
            } else {
                loadMan()
            }
        #else
            loadMan()
        #endif
        
    }
    
    func saveValue() {
        patient.email = textfieldEmail.getText()
        patient.homePhone = textfieldHomePhone.getText()
        patient.workPhone = textfieldWorkPhone.getText()
        patient.cellPhone = textfieldCellPhone.getText()
        patient.socialSecurity = textfieldSSN.getText()
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldEmail.isEmpty && !textfieldEmail.text!.isValidEmail{
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        }else if !textfieldHomePhone.isEmpty && !textfieldHomePhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID HOME PHONE NUMBER")
        }else if !textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else if !textfieldCellPhone.isEmpty && !textfieldCellPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID CELL PHONE NUMBER")
        }else if !textfieldSSN.isEmpty && !textfieldSSN.text!.isSocialSecurityNumber{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else{
            saveValue()
            let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient3VC") as! NewPatient3ViewController
            patient.patient = self.patient
            self.navigationController?.pushViewController(patient, animated: true)
        }
        
    }
    
}
