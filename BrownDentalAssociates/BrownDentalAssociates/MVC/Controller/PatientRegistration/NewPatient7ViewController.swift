//
//  NewPatient7ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/21/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient7ViewController: MCViewController {
    
    var isPrimaryInsurance : Bool!
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var textfieldCompanyName : MCTextField!
    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textfieldCity :MCTextField!
    @IBOutlet weak var textfieldState : MCTextField!
    @IBOutlet weak var textfieldZipcode : MCTextField!
    @IBOutlet weak var textfieldGroupId : MCTextField!
    @IBOutlet weak var textfieldMemberId : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldZipcode.textFormat = .Zipcode
        textfieldState.textFormat = .State
        labelTitle.text = isPrimaryInsurance == true ? "PRIMARY INSURANCE" : "SECONDARY INSURANCE"
        textfieldGroupId.textFormat = .Number
        textfieldMemberId.textFormat = .Number
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        let ins : Insurance = isPrimaryInsurance == true ? patient.primaryInsurance : patient.secondaryInsurance
        
        func loadVal(){
            textfieldGroupId.setSavedText(text: ins.groupNumber)
            textfieldMemberId.setSavedText(text: ins.memberID)
        }
        
        #if AUTO
            if let patientDetails = patient.patientDetails {
                if let insuranceAuto = isPrimaryInsurance == true ? patientDetails.primaryDentalInsurance : patientDetails.secondaryDentalInsurance {
                    textfieldMemberId.setSavedText(text: ins.memberID != nil ? ins.memberID :  insuranceAuto.insuredID)
                    textfieldGroupId.setSavedText(text: ins.groupNumber != nil ? ins.groupNumber :  insuranceAuto.groupNumber)
                } else {
                    loadVal()
                }
            } else {
                loadVal()
            }
        #else
            textfieldCompanyName.setSavedText(text: ins.insuranceCompanyName)
            textfieldAddress.setSavedText(text: ins.insuranceCompanyAddress)
            textfieldCity.setSavedText(text: ins.insuranceCompanyCity)
            textfieldState.setSavedText(text: ins.insuranceCompanyState)
            textfieldZipcode.setSavedText(text: ins.insuranceCompanyZipcode)
            
        #endif
        
    }
    
    func saveValue (){
        let ins : Insurance = isPrimaryInsurance == true ? patient.primaryInsurance : patient.secondaryInsurance
        ins.insuranceCompanyName = textfieldCompanyName.getText()
        ins.insuranceCompanyAddress = textfieldAddress.getText()
        ins.insuranceCompanyCity = textfieldCity.getText()
        ins.insuranceCompanyState = textfieldState.getText()
        ins.insuranceCompanyZipcode = textfieldZipcode.getText()
        ins.groupNumber = textfieldGroupId.getText()
        ins.memberID = textfieldMemberId.getText()
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldZipcode.isEmpty && !textfieldZipcode.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else{
            saveValue()
            if isPrimaryInsurance == true{
                CustomAlert.yesOrNoAlertView().showWithQuestion("DO YOU HAVE SECONDARY INSURANCE", completion: { (index) in
                    if index == 1 {
                        let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient6VC") as! NewPatient6ViewController
                        patient.isPrimaryInsurance = false
                        patient.patient = self.patient
                        self.navigationController?.pushViewController(patient, animated: true)
                    }else{
                        let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient8VC") as! NewPatient8ViewController
                        patient.patient = self.patient
                        self.navigationController?.pushViewController(patient, animated: true)
                    }
                })
            }else{
                let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient8VC") as! NewPatient8ViewController
                patient.patient = self.patient
                self.navigationController?.pushViewController(patient, animated: true)
            }
        }
        
    }



}
