//
//  NewPatient9ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/21/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient9ViewController: MCViewController {

    @IBOutlet weak var textfieldName1 : MCTextField!
    @IBOutlet weak var textfieldRelationship1 : MCTextField!
    @IBOutlet weak var textfieldIdentifier1 : MCTextField!
    @IBOutlet weak var textfieldName2 : MCTextField!
    @IBOutlet weak var textfieldRelationship2 : MCTextField!
    @IBOutlet weak var textfieldIdentifier2 : MCTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldName1.setSavedText(text: patient.name1)
        textfieldRelationship1.setSavedText(text: patient.relationship1)
        textfieldIdentifier1.setSavedText(text: patient.identifier1)
        textfieldName2.setSavedText(text: patient.name2)
        textfieldRelationship2.setSavedText(text: patient.relationship2)
        textfieldIdentifier2.setSavedText(text: patient.identifier2)
    }
    
    func saveValue (){
        patient.name1 = textfieldName1.getText()
        patient.relationship1 = textfieldRelationship1.getText()
        patient.identifier1 = textfieldIdentifier1.getText()
        patient.name2 = textfieldName2.getText()
        patient.relationship2 = textfieldRelationship2.getText()
        patient.identifier2 = textfieldIdentifier2.getText()
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
            saveValue()
            let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient10VC") as! NewPatient10ViewController
            patient.patient = self.patient
            self.navigationController?.pushViewController(patient, animated: true)
    }

}
