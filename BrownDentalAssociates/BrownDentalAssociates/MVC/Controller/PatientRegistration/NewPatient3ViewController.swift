//
//  NewPatient3ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/20/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient3ViewController: MCViewController {

    @IBOutlet weak var textfieldFirstName : MCTextField!
    @IBOutlet weak var textfieldLastName : MCTextField!
    @IBOutlet weak var textfieldInitial : MCTextField!
    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textfieldCity : MCTextField!
    @IBOutlet weak var textfieldState : MCTextField!
    @IBOutlet weak var textfieldZipcode : MCTextField!
    @IBOutlet weak var dropdownGender : BRDropDown!

    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldInitial.textFormat = .MiddleInitial
        textfieldState.textFormat = .State
        textfieldZipcode.textFormat = .Zipcode
        dropdownGender.items = ["MALE","FEMALE"]
        dropdownGender.placeholder = "-- SELECT GENDER --"
        loadvalue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadvalue (){
        textfieldFirstName.setSavedText(text: patient.responsibleFirstName)
        textfieldLastName.setSavedText(text: patient.responsibleLastName)
        textfieldInitial.setSavedText(text: patient.responsibleInitial)
        textfieldAddress.setSavedText(text: patient.responsibleAddress)
        textfieldCity.setSavedText(text: patient.responsibleCity)
        textfieldState.setSavedText(text: patient.responsibleState)
        textfieldZipcode.setSavedText(text: patient.responsibleZipcode)
        dropdownGender.selectedIndex = patient.responsibleGenderTag
    }
    
    func saveValue() {
        patient.responsibleFirstName = textfieldFirstName.getText()
        patient.responsibleLastName = textfieldLastName.getText()
        patient.responsibleInitial = textfieldInitial.getText()
        patient.responsibleAddress = textfieldAddress.getText()
        patient.responsibleCity = textfieldCity.getText()
        patient.responsibleState = textfieldState.getText()
        patient.responsibleZipcode = textfieldZipcode.getText()
        patient.responsibleGenderTag = dropdownGender.selectedOption == nil ? 0 : dropdownGender.selectedIndex
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldZipcode.isEmpty && !textfieldZipcode.text!.isZipCode{
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        }else{
            saveValue()
            let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatient4VC") as! NewPatient4ViewController
            patient.patient = self.patient
            self.navigationController?.pushViewController(patient, animated: true)
        }
        
    }

}
