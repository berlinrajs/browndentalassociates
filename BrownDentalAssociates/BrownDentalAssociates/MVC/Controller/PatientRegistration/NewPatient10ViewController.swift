//
//  NewPatient10ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/21/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class NewPatient10ViewController: MCViewController {
    
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet      var signatureInitial : [SignatureView]!
    @IBOutlet weak var labelPatientName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        patient.signatureInitials =  [UIImage]()
        labelPatientName.text = patient.fullName
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            for imgvw in signatureInitial{
                let image = imgvw.signatureImage() == nil ? UIImage() : imgvw.signatureImage()
                self.patient.signatureInitials.append(image!)
            }
            self.patient.signPatient = signaturePatient.signatureImage()
            let patient = patientStoryBoard.instantiateViewController(withIdentifier: "NewPatientFormVC") as! NewPatientFormViewController
            patient.patient = self.patient
            self.navigationController?.pushViewController(patient, animated: true)
        }
    }
    
    
}
