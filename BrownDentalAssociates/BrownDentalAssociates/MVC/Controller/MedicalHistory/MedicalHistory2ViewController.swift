//
//  MedicalHistory2ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/22/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory2ViewController: MCViewController {
    @IBOutlet weak var textfieldPhysicianName : MCTextField!
    @IBOutlet weak var textfieldPhysicianAddress : MCTextField!
    @IBOutlet weak var textfieldLastExam : MCTextField!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet weak var radioPMS : RadioButton!
    @IBOutlet weak var radioBirthControl : RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldLastExam.textFormat = .DateInCurrentYear
        loadValue()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldPhysicianName.setSavedText(text: patient.medicalHistory.physicianName)
        textfieldPhysicianAddress.setSavedText(text: patient.medicalHistory.physicianAddress)
        textfieldLastExam.setSavedText(text: patient.medicalHistory.lastExam)
        radioPregnant.setSelectedWithTag(patient.medicalHistory.pregantTag)
        radioPMS.setSelectedWithTag(patient.medicalHistory.PMSTag)
        radioBirthControl.setSelectedWithTag(patient.medicalHistory.birthControlTag)
    }
    
    func saveValue (){
        patient.medicalHistory.physicianName = textfieldPhysicianName.getText()
        patient.medicalHistory.physicianAddress = textfieldPhysicianAddress.getText()
        patient.medicalHistory.lastExam = textfieldLastExam.getText()
        patient.medicalHistory.pregantTag = radioPregnant.selected == nil ? 0 : radioPregnant.selected.tag
        patient.medicalHistory.PMSTag = radioPMS.selected == nil ? 0 : radioPMS.selected.tag
        patient.medicalHistory.birthControlTag = radioBirthControl.selected == nil ? 0 : radioBirthControl.selected.tag
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
            saveValue()
        let medical = medicalStoryBoard.instantiateViewController(withIdentifier: "MedicalHistory3VC") as! MedicalHistory3ViewController
        medical.patient = self.patient
        self.navigationController?.pushViewController(medical, animated: true)
    }
    
    


}
