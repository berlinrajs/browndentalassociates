//
//  MedicalHistory1ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/22/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory1ViewController: MCViewController {
    
    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet var buttonNext: MCButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelTitle : UILabel!
    var selectedIndex: Int = 0
    var arrayTitleAlert : [[String]]!
    var fromPreviousForm : Bool =  false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.activityIndicator.stopAnimating()
        arrayTitleAlert = [["","If so, what is the condition being treated?", "If so, what was the illness?" , "If so what was the problem?","","","If so, explain the circumstances and when"],["If so, how much per day and what?","If so how much per day/week/month and what?","If so, how much per day and what?",""]]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        self.fromPreviousForm = isFromPreviousForm
    }
    
    
    @IBAction func onBackButton1Pressed (withSender sender: AnyObject) {
        if selectedIndex == 0 {
            let _ = self.navigationController?.popViewController(animated: true)
        } else {
            buttonBack?.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            
            self.activityIndicator.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                self.activityIndicator.stopAnimating()
                self.selectedIndex = self.selectedIndex - 1
                self.labelTitle.text = self.selectedIndex == 0 || self.selectedIndex == 1 ? "" : self.selectedIndex == 2 || self.selectedIndex == 3 ? "Do you have or have you had any of the following disease or problems" : self.selectedIndex == 4 ? "Are you allergic or have you reacted adversely to :" : "Are you taking any of the following :"
                self.tableViewQuestions.reloadData()
                if self.fromPreviousForm  && self.selectedIndex == 0
                {
                   self.buttonBack?.isHidden = true
                }
                self.buttonBack?.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
            }
        }
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    func gotoNextPage (){
        let medical = medicalStoryBoard.instantiateViewController(withIdentifier: "MedicalHistory2VC") as! MedicalHistory2ViewController
        medical.patient = self.patient
        self.navigationController?.pushViewController(medical, animated: true)
        
    }
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            if selectedIndex == 5 {
                if checkforSelection() == true{
                    PopupTextField.popUpView().showInViewController(self, WithTitle: "State drug name, dosage and frequency", placeHolder: "PLEASE SPECIFY", textFormat: TextFormat.Default, completion: { (popUp, textField) in
                        self.patient.medicalHistory.dosage = textField.isEmpty ? "N/A" : textField.text!
                        popUp.close()
                        self.gotoNextPage()
                    })
                }else{
                    gotoNextPage()
                }
            } else {
                buttonBack?.isUserInteractionEnabled = false
                buttonNext.isUserInteractionEnabled = false
                self.buttonVerified.isSelected = false
                self.activityIndicator.startAnimating()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
          
                    self.activityIndicator.stopAnimating()
                    self.selectedIndex = self.selectedIndex + 1
                    self.labelTitle.text = self.selectedIndex == 0 || self.selectedIndex == 1 ? "" : self.selectedIndex == 2 || self.selectedIndex == 3 ? "Do you have or have you had any of the following disease or problems" : self.selectedIndex == 4 ? "Are you allergic or have you reacted adversely to :" : "Are you taking any of the following :"
                    if self.selectedIndex == 1
                    {
                        self.buttonBack?.isHidden = false
                    }
                    
                    self.tableViewQuestions.reloadData()
                    self.buttonBack?.isUserInteractionEnabled = true
                    self.buttonNext.isUserInteractionEnabled = true
                }
            }
        }
    }
    
    func checkforSelection() -> Bool{
        var success : Bool = false
        for ques in patient.medicalHistory.medicalQuestions[5]{
            if ques.selectedOption == true{
                success = true
                return success
            }
        }
        return success
    }
    
}

extension MedicalHistory1ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.medicalHistory.medicalQuestions[selectedIndex].count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (selectedIndex == 2 && indexPath.row == 2) || selectedIndex == 0 || selectedIndex == 1 ? 55 : 45
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(question: self.patient.medicalHistory.medicalQuestions[selectedIndex][indexPath.row])
        return cell
    }
}

extension MedicalHistory1ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(cell: PatientInfoCell) {
        if selectedIndex == 1 && cell.tag == 3{
            PopupTextView.popUpView().showWithPlaceHolder("IF SO, PLEASE EXPLAIN", completion: { (popUp, textView) in
                if textView.isEmpty{
                    cell.radioButtonYes.setSelected(false)
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
                }else{
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = textView.text
                }
                popUp.close()
            })
        }else{
            let placeHolder  =  "PLEASE EXPLAIN"
            PopupTextField.popUpView().showInViewController(self, WithTitle: selectedIndex > 1 ? "" : arrayTitleAlert[selectedIndex][cell.tag], placeHolder: placeHolder, textFormat: TextFormat.Default) { (popUpView, textField) in
                if textField.isEmpty{
                    cell.radioButtonYes.setSelected(false)
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
                }else{
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = textField.text
                }
                popUpView.close()
            }
        }
    }
}
