//
//  MedicalHistory3ViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/22/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistory3ViewController: MCViewController {

    @IBOutlet weak var textviewRemarks : MCTextView!
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate1 : DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelPatientName.text = patient.fullName
        textviewRemarks.placeholder = "PLEASE TYPE HERE"
        textviewRemarks.placeholderColor = UIColor.lightGray
        labelDate1.todayDate = patient.dateToday
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValue (){
        textviewRemarks.text = patient.medicalHistory.remarks == "N/A" ? "PLEASE TYPE HERE" : patient.medicalHistory.remarks
        textviewRemarks.textColor = patient.medicalHistory.remarks == "N/A" ? UIColor.lightGray : UIColor.black
    }
    
    func saveValue (){
        patient.medicalHistory.remarks = textviewRemarks.isEmpty ? "N/A" : textviewRemarks.text!
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if  !signaturePatient.isSigned(){
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            saveValue()
            patient.medicalHistory.signPatient = signaturePatient.signatureImage()
            let medical = medicalStoryBoard.instantiateViewController(withIdentifier: "MedicalHistoryFormVC") as! MedicalHistoryFormViewController
            medical.patient = self.patient
            self.navigationController?.pushViewController(medical, animated: true)
        }
    }

    
}
