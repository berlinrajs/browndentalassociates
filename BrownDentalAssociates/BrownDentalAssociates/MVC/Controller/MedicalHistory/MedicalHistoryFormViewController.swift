//
//  MedicalHistoryFormViewController.swift
//  BrownDentalAssociates
//
//  Created by Bala Murugan on 1/22/17.
//  Copyright © 2017 Bala Murugan. All rights reserved.
//

import UIKit

class MedicalHistoryFormViewController: MCViewController {

    @IBOutlet weak var labelName  : UILabel!
    @IBOutlet weak var labelBirthDate : UILabel!
    @IBOutlet      var labelPhysicianName : [UILabel]!
    @IBOutlet weak var labelPhysicianLastVisit : UILabel!
    @IBOutlet weak var labelPhysicianCare : UILabel!
    @IBOutlet weak var labelIllness : UILabel!
    @IBOutlet weak var labelHospitalized : UILabel!
    @IBOutlet weak var labelCircumstances : UILabel!
    @IBOutlet weak var labelOther : UILabel!
    @IBOutlet weak var labelDrugName : UILabel!
    @IBOutlet weak var labelLatex : UILabel!
    @IBOutlet weak var labelTobacco : UILabel!
    @IBOutlet weak var labelAlcohol : UILabel!
    @IBOutlet weak var labelCaffeinated : UILabel!
    @IBOutlet      var labelDisease : [UILabel]!
    @IBOutlet weak var labelRemarks : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet      var radioButtons1 : [RadioButton]!
    @IBOutlet      var radioButtons2 : [RadioButton]!
    @IBOutlet      var radioButtons3 : [RadioButton]!
    @IBOutlet      var radioButtons4 : [RadioButton]!
    @IBOutlet      var radioButtons5 : [RadioButton]!
    @IBOutlet      var radioButtons6 : [RadioButton]!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet weak var radioPMS : RadioButton!
    @IBOutlet weak var radioBirthcontrol : RadioButton!

    
    func formatPhysicianName() -> String  {
        let strArray = [patient.medicalHistory.physicianName,patient.medicalHistory.physicianAddress]
        var array : [String] = [String]()
        for str in strArray{
            if str != "N/A"{
                array.append(str)
            }
        }
        return array.count == 0 ? "N/A" : (array as NSArray).componentsJoined(by: ", ")
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = patient.fullName
        labelBirthDate.text = patient.dateOfBirth
        self.formatPhysicianName().setTextForArrayOfLabels(labelPhysicianName)
        labelPhysicianLastVisit.text = patient.medicalHistory.lastExam
        labelPhysicianCare.text = patient.medicalHistory.medicalQuestions[0][1].answer
        labelIllness.text = patient.medicalHistory.medicalQuestions[0][2].answer
        labelHospitalized.text = patient.medicalHistory.medicalQuestions[0][3].answer
        labelCircumstances.text = patient.medicalHistory.medicalQuestions[0][6].answer
        labelOther.text = patient.medicalHistory.medicalQuestions[3][13].answer
        labelDrugName.text = patient.medicalHistory.dosage
        labelLatex.text = patient.medicalHistory.medicalQuestions[4][7].answer
        labelTobacco.text = patient.medicalHistory.medicalQuestions[1][0].answer
        labelAlcohol.text = patient.medicalHistory.medicalQuestions[1][1].answer
        labelCaffeinated.text = patient.medicalHistory.medicalQuestions[1][2].answer
        let str =  patient.medicalHistory.medicalQuestions[1][3].answer == nil ? "" : patient.medicalHistory.medicalQuestions[1][3].answer
        str!.setTextForArrayOfLabels(labelDisease)
        labelRemarks.text = patient.medicalHistory.remarks
        labelDate1.text = patient.dateToday
        signaturePatient.image = patient.medicalHistory.signPatient
        for (idx,btn) in radioButtons1.enumerated(){
            btn.isSelected = patient.medicalHistory.medicalQuestions[0][idx].selectedOption
        }
        for (idx,btn) in radioButtons2.enumerated(){
            btn.isSelected = patient.medicalHistory.medicalQuestions[1][idx].selectedOption
        }
        for (idx,btn) in radioButtons3.enumerated(){
            btn.isSelected = patient.medicalHistory.medicalQuestions[2][idx].selectedOption
        }
        for (idx,btn) in radioButtons4.enumerated(){
            btn.isSelected = patient.medicalHistory.medicalQuestions[3][idx].selectedOption
        }
        for (idx,btn) in radioButtons5.enumerated(){
            btn.isSelected = patient.medicalHistory.medicalQuestions[4][idx].selectedOption
        }
        for (idx,btn) in radioButtons6.enumerated(){
            btn.isSelected = patient.medicalHistory.medicalQuestions[5][idx].selectedOption
        }

        radioPregnant.setSelectedWithTag(patient.medicalHistory.pregantTag)
        radioPMS .setSelectedWithTag(patient.medicalHistory.PMSTag)
        radioBirthcontrol.setSelectedWithTag(patient.medicalHistory.birthControlTag)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
