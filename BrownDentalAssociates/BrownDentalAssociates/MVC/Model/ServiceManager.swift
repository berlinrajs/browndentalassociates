//
//  ServiceManager.swift
//   Angell Family Dentistry
//
//  Created by Bose on 02/02/16.
//  Copyright © 2016  Angell Family Dentistry. All rights reserved.
//

import UIKit

let kAccessToken = "ServerAccessToken"

class ServiceManager: NSObject {
    
    class var serverUrl: String {
//        return "http://192.168.0.200:8080/"
        return "http://\(UserDefaults.standard.value(forKey: "hostIP")!):8080/"
    }
    
    class func fetchDataFromService(_ baseUrlString: String, serviceName: String, parameters : [String : String]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: baseUrlString))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(serviceName, parameters: parameters, progress: { (progress) in
            }, success: { (task, result) in
                success(result as AnyObject)
        }) { (task, error) in
            failure(error as NSError)
        }
    }
    
    class func fetchDataFromServiceCheckIn(_ serviceName: String, parameters : [String : String]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : NSError) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: "https://alpha.mncell.com/mconsent/"))
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(serviceName, parameters: parameters, progress: { (progress) in
            }, success: { (task, result) in
                success(result as AnyObject)
        }) { (task, error) in
            failure(error as NSError)
        }
    }
    class func loginWithUsername(_ userName: String, password: String, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        ServiceManager.fetchDataFromService("https://mncell.com/mclogin/", serviceName: "apploginapi.php?", parameters: ["appkey": kAppKey, "username": userName, "password": password], success: { (result) in
            if (result["posts"] as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }
    class func ChekInFormStatus(_ patientName: String, patientPurpose: String, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        //"mcdistinctivelaser"
        ServiceManager.fetchDataFromServiceCheckIn("savepatients_info.php?", parameters: ["patientkey": kAppKey, "patientname": patientName, "patientpurpose": patientPurpose], success: { (result) in
            if (result["posts"] as! String == "success") {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, nil)
        }
    }
    
    #if AUTO
    class func getDataFromServer(serviceName: String, parameters : [String : String]?, success: @escaping (_ result : AnyObject) -> Void, failure : @escaping (_ error : Error) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: serverUrl))
        manager.responseSerializer.acceptableContentTypes = ["text/html", "application/json"]
        if let accessToken = UserDefaults.standard.value(forKey: kAccessToken) as? String {
            manager.requestSerializer.setValue(accessToken, forHTTPHeaderField: "Authorization")
        }
        manager.get(serviceName, parameters: parameters, progress: { (progress) in
            
        }, success: { (task, result) in
            if result != nil {
                
                success(result as AnyObject)
                
            } else {
                
                failure(NSError(errorMessage: "Something went wrong"))
                
            }
        }) { (task, error) in
            failure(error)
        }
    }
    
    class func postDataToServer(serviceName: String, parameters : [String : String]?, success: @escaping(_ result : AnyObject) -> Void, failure : @escaping(_ error : Error) -> Void) {
        
        let manager = AFHTTPSessionManager(baseURL: URL(string: serverUrl))
        manager.responseSerializer.acceptableContentTypes = ["text/html", "application/json"]
        if let accessToken = UserDefaults.standard.value(forKey: kAccessToken) as? String {
            manager.requestSerializer.setValue(accessToken, forHTTPHeaderField: "Authorization")
        }
        manager.post(serviceName, parameters: parameters, progress: { (progress) -> Void in
            
        }, success: { (task, result) -> Void in
            if result != nil {
                if let accessToken = (result as AnyObject)["access_token"] as? String {
                    UserDefaults.standard.set("Bearer \(accessToken)", forKey: kAccessToken)
                    UserDefaults.standard.synchronize()
                }
                success(result as AnyObject)
            } else {
                failure(NSError(errorMessage: "Something went wrong"))
            }
        }) { (task, error) -> Void in
            failure(error)
        }
    }
    #endif
    
    class func postReview(_ name: String, comment: String, rating: CGFloat, phoneNumber: String, allowMessage: Bool, email: String, anonymous: Bool, completion: @escaping (_ success: Bool, _ error: NSError?) -> Void) {
        ServiceManager.fetchDataFromService("https://alpha.mncell.com/review/", serviceName: "app_review.php?", parameters: ["patient_appkey": kAppKey, "patient_name": name, "patient_review_comment": comment, "patient_rating": "\(rating)", "patient_phone_number": phoneNumber, "sms_allowed": allowMessage == true ? "1" : "0", "patient_email": email, "user_anonymous": anonymous == true ? "1" : "0"], success: { (result) in
            if result["posts"] != nil && (result["posts"] as AnyObject)["status"] as! String == "success" {
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: (result["posts"] as AnyObject)["message"] as! String]))
            }
        }) { (error) in
            completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, Please Check your internet connection and try again"]))
        }
    }
    
    class func uploadFile(patientId: String?, fileName: String, fileUrl: String, completion: @escaping (_ success: Bool, _ errorMessage: String?)-> Void) {
        
        func uploadFile() {
            //        https://consentforms.mncell.com/consent_form_upload_to_server.php
            
            let manager = AFHTTPSessionManager(baseURL: URL(string: "https://consentforms.mncell.com/"))
            
            manager.responseSerializer.acceptableContentTypes = ["text/html"]
            manager.post("consent_form_upload_to_server.php", parameters: ["appkey": kAppKey], constructingBodyWith: { (data) in
                data.appendPart(withFileData: try! Data(contentsOf: URL(fileURLWithPath: fileUrl)), name: "consent_file", fileName: fileName, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                if let postDict = (result as! [String: AnyObject])["posts"] as? [String: AnyObject] {
                    if postDict["status"]?.lowercased == "success" {
                        completion(true, nil)
                    } else {
                        completion(false, postDict["message"] as? String)
                    }
                } else {
                    completion(false, "SOMETHING WENT WRONG, PLEASE CHECK YOUR INTERNET CONNECTION AND TRY AGAIN LATER")
                }
            }) { (task, error) in
                completion(false, error.localizedDescription)
            }
        }
        
        
        #if AUTO
            let folderNames = [kNewPatientSignInForm: "Patient Information", kMedicalHistory: "Medical History", kInsuranceCard: "Patient Insurance", kDrivingLicense: "Patient Information"]
            
            var folderName : String {
                get {
                    for (key, value) in folderNames {
                        if fileName.contains(key.fileName) {
                            return value
                        }
                    }
                    return "CONSENT FORMS"
                }
            }
            
            
            let params = ["patientId": patientId!, "fileName": folderName]
            let manager = AFHTTPSessionManager(baseURL: URL(string: serverUrl))
            manager.responseSerializer.acceptableContentTypes = ["text/html", "application/json"]
            if let accessToken = UserDefaults.standard.value(forKey: kAccessToken) as? String {
                manager.requestSerializer.setValue(accessToken, forHTTPHeaderField: "Authorization")
            }
            manager.post("api/patients", parameters: params, constructingBodyWith: { (formData) in
                do {
                    try formData.appendPart(withFileURL: URL(fileURLWithPath: fileUrl), name: "file", fileName: fileName, mimeType: "application/pdf")
                } catch {
                    
                }
            }, progress: { (progress) in
                
            }, success: { (task, result) in
                print("RESULT: \(result)")
                uploadFile()
            }, failure: { (task, error) in
                print(error)
                uploadFile()
            })
        #else
            uploadFile()
        #endif
    }
}
