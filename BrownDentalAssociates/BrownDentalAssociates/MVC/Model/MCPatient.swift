//
//  MCPatient.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class MCPatient: NSObject {
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
    #if AUTO
    var patientDetails : PatientDetails?
    
    #endif
    
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var initial : String = ""
    var preferredName : String!
    
    var dateOfBirth : String!
    var dateToday : String!
    var dentistName: String!
    
    var parentFirstName: String!
    var parentLastName: String!
    var parentDateOfBirth: String!
    
    var is18YearsOld : Bool {
        return patientAge >= 18
    }
    
    var fullName: String {
        return initial.characters.count > 0 ? firstName + " " + initial + " " + lastName : firstName + " " + lastName
    }
    var patientAge: Int! {
        get {
            if dateOfBirth == nil || dateOfBirth.characters.count == 0 {
                return 0
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = kCommonDateFormat
            
            let numberOfDays = (Date().timeIntervalSince(dateFormatter.date(from: self.dateOfBirth)!))/(3600 * 24)
            return Int(numberOfDays/365.2425)
        }
    }
    
    var address: String!
    var city: String!
    var state: String = kState
    var zipcode: String!
    var genderTag : Int = 0
    var driverLicense : String!
    
    var email : String!
    var homePhone : String!
    var workPhone : String!
    var cellPhone : String!
    var socialSecurity : String!
    
    var responsibleFirstName : String!
    var responsibleLastName : String!
    var responsibleInitial : String!
    var responsibleAddress : String!
    var responsibleCity : String!
    var responsibleState : String!
    var responsibleZipcode : String!
    var responsibleGenderTag : Int = 0
    
    var responsibleEmail : String!
    var responsibleHomePhone : String!
    var responsibleWorkPhone : String!
    var responsibleCellPhone : String!
    var responsibleSocialSecurity : String!
    var responsibleDriverLicense : String!
    var responsibleBirthdate : String!
    var responsibleMaritalStatus : Int = 0
    
    var spouseName : String!
    var spouseHomePhone : String!
    var spouseWorkPhone : String!
    var spouseCellPhone : String!
    var spouseSocialSecurity : String!
    var spouseDriverLicense : String!
    var spouseBirthdate : String!
    
    
    var primaryInsurance: Insurance = Insurance()
    var secondaryInsurance: Insurance = Insurance()
    
    var emergencyName : String!
    var emergencyPhone : String!
    var emergencyRelation : String!
    var chooseBrownDental : String!
    var reference : String!
    
    var name1 : String!
    var name2 : String!
    var relationship1 : String!
    var relationship2 : String!
    var identifier1 : String!
    var identifier2 : String!
    
    var signatureInitials : [UIImage] = [UIImage]()
    var signPatient : UIImage!
    
    var visitorFirstName : String!
    var visitorLastName : String!
    
    var visitorFullName : String {
        return visitorFirstName + " " + visitorLastName
    }
    var visitingPurpose : String!
    
    var signatureFinancialPolicy: UIImage!
    var signatureAppointmentPolicy: UIImage!
    var signatureReminderPolicy: UIImage!
    
    var reminderPhoneNumber: String!
    var reminderEmail: String!
    var isAllowText: Bool!
    var isAllowEmail: Bool!
    
    var photographyRelation : String = ""
    
    var surgeryProcedures : String = ""
    var surgeryAnestheticTag : [Int] = [Int]()
    
    var periodontitisTag : [Int] = [Int]()
    var signature1 : UIImage!
    
    var medicalHistory : MedicalHistory = MedicalHistory()

}

class Insurance: NSObject {
    var relationTag : Int = 0
    var relationOther : String!
    var nameInsured : String!
    var socialSecurity : String!
    var dateOfBirth : String!
    var employerName : String!
    var employerAddress : String!
    var employerState : String!
    var employerCity : String!
    var employerZipcode : String!
    var insuranceCompanyName : String!
    var insuranceCompanyAddress : String!
    var insuranceCompanyCity : String!
    var insuranceCompanyState : String!
    var insuranceCompanyZipcode : String!
    var groupNumber : String!
    var memberID : String!
    
}

class MedicalHistory : NSObject{
    
    var dosage : String = ""
    var medicalQuestions : [[MCQuestion]]!
    
    var physicianName : String = ""
    var physicianAddress : String = ""
    var lastExam : String = ""
    var pregantTag : Int = 0
    var PMSTag : Int = 0
    var birthControlTag : Int = 0
    
    var remarks : String = ""
    var signPatient : UIImage!
    var signDentist : UIImage!
    
    required override init() {
        super.init()
        
        let quest1 : [String] = ["Has there been any change in your general health within the past year?",
                                 "Are you now under the care of a physician?",
                                 "Have you had any serious illness within the past five (5) years?",
                                 "Have you been hospitalized or had an operation within the past five (5) years?",
                                 "Have you had abnormal bleeding associated with previous extractions, surgery or trauma?",
                                 "Do you bruise easily?",
                                 "Have you ever required a blood transfusion?",
                                 "Have you ever tested positive for the AIDS virus?",
                                 "Do you have any blood disorder such as anemia?",
                                 "Have you had surgery or x-ray treatment for a tumor, growth, or other condition?"]
        
        let quest2 : [String] = ["Do you use any tobacco products?",
                                 "Do you use any alcohol products?",
                                 "Do you use any caffeinated products (coffee, tea, chocolate, etc.)?",
                                 "Do you have any disease, condition, or problem not listed above that you think I should know about?",
                                 "Are you employed in any situation which exposes you regularly to x-rays or other ionizing radiation?",
                                 "Are you wearing contact lenses?",
                                 "Are you experiencing stress or pressure in your work at home?",
                                 "Do you use recreational drugs?"]
        
        let quest3 : [String] = ["Rheumatic fever or rheumatic heart disease",
                                 "Congenital heart disease",
                                 "Cardiovascular disease (heart trouble, heart attack, heart murmur, coronary insufficiency, coronary occlusion, high/low blood pressure, arteeriosclerosis, stroke, etc.)",
                                 "Do you have pain in chest upon exertion?",
                                 "Are you ever short of breath after mild exercise?",
                                 "Do your ankles swell?",
                                 "Do you get short of breath when you lie down, or do you require extra pillows when you sleep?",
                                 "Artificial or replacement valves",
                                 "Pacemaker",
                                 "Allergy",
                                 "Sinus trouble",
                                 "Asthma or hay fever",
                                 "Hives or a skin rash",
                                 "Fainting spells or seizures"]

        let quest4 : [String] = ["Diabetes",
                                 "Do you have to urinate (pass water) more than six times a day?",
                                 "Are you thirsty much of the time?",
                                 "Does your mouth frequently become dry?",
                                 "Hepatitis, jaundice or liver disease",
                                 "Arthritis or inflammatory rheumatism",
                                 "Arthritis or replacement joints, prosthetic",
                                 "Digestive system-Ulcers or stomach disorders (colitis)",
                                 "Kidney trouble",
                                 "Tuberculosis",
                                 "Persistent cough or cough up blood",
                                 "Immune System disorders (including AIDS, HIV, ARC)",
                                 "Venereal disease",
                                 "Other"]

        let quest5 : [String] = ["Local anesthetics",
                                 "Penicillin or other antibiotics",
                                 "Sulfa drugs",
                                 "Barbiturates, sedatives, or sleeping pills",
                                 "Aspirin",
                                 "Iodine",
                                 "Codeine or other narcotics",
                                 "Latex or other"]

        let quest6 : [String] = ["Antibiotics or sulfa drugs",
                                 "Anticoagulants (blood thinners)",
                                 "Medicine for high blood pressure",
                                 "Cortisone (steroids)",
                                 "Tranquilizers",
                                 "Antihistamines",
                                 "Aspirin",
                                 "Insulin, tolbutamide (Orinase) or similar drug for diabetes",
                                 "Digitalis or drugs for heart trouble",
                                 "Nitroglycerin",
                                 "Other medications"]
        


        self.medicalQuestions = [MCQuestion.getArrayOfQuestions(questions: quest1),MCQuestion.getArrayOfQuestions(questions: quest2),MCQuestion.getArrayOfQuestions(questions: quest3),MCQuestion.getArrayOfQuestions(questions: quest4),MCQuestion.getArrayOfQuestions(questions: quest5),MCQuestion.getArrayOfQuestions(questions: quest6)]
        self.medicalQuestions[0][1].isAnswerRequired = true
        self.medicalQuestions[0][2].isAnswerRequired = true
        self.medicalQuestions[0][3].isAnswerRequired = true
        self.medicalQuestions[0][6].isAnswerRequired = true

        
        self.medicalQuestions[1][0].isAnswerRequired = true
        self.medicalQuestions[1][1].isAnswerRequired = true
        self.medicalQuestions[1][2].isAnswerRequired = true
        self.medicalQuestions[1][3].isAnswerRequired = true
        
        self.medicalQuestions[3][13].isAnswerRequired = true
        self.medicalQuestions[4][7].isAnswerRequired = true

        



        
        
        
    }

    
}

#if AUTO
    
    class InsuranceAuto: NSObject {
        
        var name: String!
        var companyName: String!
        var insuredID: String!
        var relation: Relationship?
        var group: String!
        var groupNumber: String!
        
        override init() {
            super.init()
        }
        
        init(details: [String: String]) {
            super.init()
            name = details["name"]
            companyName = details["companyName"]
            insuredID = details["insuredID"]
            if details["relation"]!.isEmpty == false {
                relation = Relationship(relation: details["relation"]!)
            }
            group = details["group"]
            groupNumber = details["groupNumber"]
        }
    }
    
    enum Relationship: Int {
        case SELF = 1
        case SPOUSE
        case CHILD
        case OTHER
        
        
        init(relation: String) {
            switch relation {
            case "1": self.init(rawValue: 1)!
                break
            case "2": self.init(rawValue: 2)!
                break
            case "3": self.init(rawValue: 3)!
                break
            default: self.init(rawValue: 4)!
                break
            }
        }
        
        var index: Int {
            switch self {
            case .SELF: return 1
            case .SPOUSE: return 2
            case .CHILD: return 3
            default: return 4
            }
        }
        
        var text : String {
            switch self {
            case .SELF: return "Self"
            case .SPOUSE: return "Spouse"
            case .CHILD: return "Child"
            default: return "Other"
            }
        }
    }
    
    enum MaritalStatus: Int {
        case SINGLE = 0
        case MARRIED
        case CHILD
        case WIDOWED
        case DIVORCED
    }
    
    enum Gender: Int {
        case MALE = 1
        case FEMALE
    }
    
    class PatientDetails : NSObject {
        var dateOfBirth : String!
        var firstName : String!
        var lastName : String!
        var preferredName : String! = ""
        var address : String!
        var city : String!
        var state : String!
        var zipCode : String!
        var country : String!
        var gender : Int! = 0
        var socialSecurityNumber : String!
        var email : String!
        var homePhone : String!
        var workPhone: String!
        var cellPhone: String!
        var patientNumber : String! = ""
        var maritalStatus: Int! = 0
        
        var primaryDentalInsurance: InsuranceAuto?
        var secondaryDentalInsurance: InsuranceAuto?
        var primaryMedicalInsurance: InsuranceAuto?
        var secondaryMedicalInsurance: InsuranceAuto?
        
        init(details : [String: AnyObject]) {
            super.init()
            self.city = getValue(details["City"])
            self.dateOfBirth = getValue(details["DateOfBirth"])
            self.email = getValue(details["Email"])
            self.socialSecurityNumber = getValue(details["SocialSecurity"])
            self.address = getValue(details["Address"])
            self.zipCode = getValue(details["Zip"])
            self.gender = getValue(details["Gender"]) == "" ? nil : Int(getValue(details["Gender"]))
            self.patientNumber = getValue(details["Id"])
            self.state = getValue(details["State"])
            self.homePhone = getValue(details["HomePhone"]).formattedPhoneNumber
            self.workPhone = getValue(details["WorkPhone"]).formattedPhoneNumber
            self.cellPhone = getValue(details["CellPhone"]).formattedPhoneNumber
            self.lastName = getValue(details["LastName"])
            self.firstName = getValue(details["FirstName"])
            
            
            func getName(_ nameString: String) -> String {
                var fullName = nameString
                let names = fullName.components(separatedBy: "_")
                if names.count == 2 {
                    let firstName = getValue(names[0] as AnyObject?)
                    let lastName = getValue(names[1] as AnyObject?)
                    fullName = "\(firstName) \(lastName)"
                }
                return fullName
            }
            
            
            if getValue(details["PrimaryDentalinsuranceAvailable"]) == "YES" {
                let insuranceDetails = ["name": getName(getValue(details["PrimaryDentalinsuranceSubName"])), "insuredID": getValue(details["PrimaryDentalinsuranceSubId"]), "companyName": getValue(details["PrimaryDentalinsuranceCarrierName"]), "relation": getValue(details["PrimaryDentalinsuranceRelation"]), "group": getValue(details["PrimDentalinsuranceGroupPlan"]), "groupNumber": getValue(details["PrimaryDentalinsuranceGroupId"])]
                self.primaryDentalInsurance = InsuranceAuto(details: insuranceDetails)
            }
            
            if getValue(details["SecondaryDentalinsuranceAvailable"]) == "YES" {
                
                let insuranceDetails = ["name": getName(getValue(details["SecondaryDentalinsuranceSubName"])), "insuredID": getValue(details["SecondaryDentalinsuranceSubId"]), "companyName": getValue(details["SecondaryDentalinsuranceCarrierName"]), "relation": getValue(details["SecondaryDentalinsuranceRelation"]), "group": getValue(details["SecondaryDentalinsuranceGroupPlan"]), "groupNumber": getValue(details["SecondaryDentalinsuranceGroupId"])]
                self.secondaryDentalInsurance = InsuranceAuto(details: insuranceDetails)
            }
            
            if getValue(details["PrimaryMedicalinsuranceAvailable"]) == "YES" {
                let insuranceDetails = ["name": getName(getValue(details["PrimaryMedicalinsuranceSubName"])), "insuredID": getValue(details["PrimaryMedicalinsuranceSubId"]), "companyName": getValue(details["PrimaryMedicalinsuranceCarrierName"]), "relation": getValue(details["PrimaryMedicalinsuranceRelation"]), "group": getValue(details["PrimMedicalinsuranceGroupPlan"]), "groupNumber": getValue(details["PrimaryMedicalinsuranceGroupId"])]
                self.primaryMedicalInsurance = InsuranceAuto(details: insuranceDetails)
            }
            
            if getValue(details["SecondaryMedicalinsuranceAvailable"]) == "YES" {
                let insuranceDetails = ["name": getName(getValue(details["SecondaryMedicalinsuranceSubName"])), "insuredID": getValue(details["SecondaryMedicalinsuranceSubId"]), "companyName": getValue(details["SecondaryMedicalinsuranceCarrierName"]), "relation": getValue(details["SecondaryMedicalinsuranceRelation"]), "group": getValue(details["SecondaryMedicalinsuranceGroupPlan"]), "groupNumber": getValue(details["SecondaryMedicalinsuranceGroupId"])]
                self.secondaryMedicalInsurance = InsuranceAuto(details: insuranceDetails)
            }
        }
        
        func getValue(_ value: AnyObject?) -> String {
            if value is String {
                let characterSet = NSCharacterSet.whitespaces
                return (value as! String).trimmingCharacters(in: characterSet).uppercased()
            }
            return ""
        }
    }
#endif




