//
//  Constants.swift
//  MConsentForms
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

public enum TextFormat : Int {
    case Default = 0
    case SocialSecurity
    case ToothNumber
    case Phone
    case ExtensionCode
    case Zipcode
    case Number
    case Date
    case Month
    case Year
    case DateInCurrentYear
    case DateIn1980
    case Time
    case MiddleInitial
    case State
    case Amount
    case Email
    case SecureText
    case AlphaNumeric
    case Username 
}

//KEYS
let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"

let kAppLoggedInKey = "kApploggedIn"
let kAppLoginUsernameKey = "kAppLoginUserName"
let kAppLoginPasswordKey = "kAppLoginPassword"


//VARIABLES
#if AUTO
let kKeychainItemLoginName = "Brown Dental Associatives Auto: Google Login"
let kKeychainItemName = "Brown Dental Associatives Auto: Google Drive"
let kClientId = "352611367423-gr5l2bi8u7kom4emisugfqqinqap18ro.apps.googleusercontent.com"
let kClientSecret = "aUTrNIFOuHHtHj2HZBp1-TaF"
let kFolderName = "BrownDentalAssociativesAuto"
#else
let kKeychainItemLoginName = "Brown Dental Associatives: Google Login"
let kKeychainItemName = "Brown Dental Associatives: Google Drive"
let kClientId = "988158855230-pi5c6df0fkr3qf0mv72c020jf2ols62g.apps.googleusercontent.com"
let kClientSecret = "twJNrMh7EqDaBh7YGk1ijsvC"
let kFolderName = "BrownDentalAssociatives"
#endif



let kDentistNames: [String] = ["DR. RONNIE BROWN", "DR. BOB BROWN", "DR. CHRISTOPHER THURSTON", "DR. TYLER TONEY", "DR. ROBERT ASHBY"]
let kDentistNameNeededForms = [kCrownAndBridges,kDentures,kSurgery,kDentureAdjustments]
let kClinicName = "BROWN DENTAL ASSOCIATES"
let kAppName = "BROWN DENTAL ASSOCIATES"
let kPlace = "ABINGDON, VA"
let kState = "VA"
let kAppKey = "mcBrown"
let kAppLoginAvailable: Bool = true
let kCommonDateFormat = "MMM dd, yyyy"

let kGoogleID = "demo@srswebsolutions.com"
let kGooglePassword = "Srsweb123#"
let kOutputFileMustBeImage: Bool = false
//END OF VARIABLES

//FORMS
let kNewPatientSignInForm = "PATIENT REGISTRATION"
let kMedicalHistory = "MEDICAL HISTORY"

//CONSENT FORMS
//let kConsentForms = "CONSENT FORMS"
let kXrayRefusal = "XRAY REFUSAL FORM"
let kEndodontic = "CONSENT FOR ENDODONTIC (ROOT CANAL) TREATMENT"
let kFinancialPolicy = "CONSENT FOR SERVICES AND FINANCIAL POLICY"
let kWhitening = "CONSENT FOR TAKE HOME WHITENING"
let kCrownAndBridges = "CROWNS AND BRIDGES CONSENT FORM"
let kPhotography = "PHOTOGRAPHY RELEASE"
let kDentures = "IMMEDIATE COMPLETE DENTURES AND PARTIAL DENTURES"
let kNitrousOxide = "NITROUS OXIDE SEDATION"
let kSurgery = "INFORMED CONSENT FOR SURGERY"
let kTreatmentFillings = "CONSENT TO TREATMENT - FILLINGS"
let kPeriodontitis = "NOTIFICATION OF GINGIVITIS/PERIODONTITIS"
let kDentureAdjustments = "PARTIAL/DENTURE ADJUSTMENTS"
let kRecareVisit = "WHY A 3 MONTH PERIODONTAL RECARE VISIT?"
let kOralScreening = "ORAL SCREENING CONSENT FORM"

let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN DRIVING LICENSE"

let kFeedBack = "CUSTOMER REVIEW FORM"
let kVisitorCheckForm = "VISITOR CHECK IN FORM"
// END OF FORMS


#if AUTO
    //let kNewPatient = ["NEW PATIENT" : [, kNoticeOfPrivacy, kNoticeOfPrivacySpan, kInsuranceCard, kDrivingLicense]]
let kExistingPatient = ["EXISTING PATIENT": [kNewPatientSignInForm,kMedicalHistory, kInsuranceCard, kDrivingLicense]]
let kConsentForms = ["CONSENT FORMS": [kEndodontic,kXrayRefusal,kFinancialPolicy,kWhitening,kCrownAndBridges,kPhotography,kDentures,kNitrousOxide,kSurgery,kTreatmentFillings,kPeriodontitis,kDentureAdjustments,kRecareVisit,kOralScreening]]
#else
let kConsentForms = "CONSENT FORMS"
let consentIndex: Int = 4
#endif

let toothNumberRequired: [String] = [kEndodontic]
//Replace '""' with form names
//let consentIndex: Int = 4


