//
//  MCTextField.swift
//  WestgateSmiles
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MCTextField: UITextField {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderColor = isEnabled ? borderColor.cgColor : borderColor.withAlphaComponent(0.2).cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: isEnabled ? placeHolderColor : placeHolderColor.withAlphaComponent(0.2)])
        tintColor = isEnabled ? placeHolderColor : placeHolderColor.withAlphaComponent(0.2)
        clipsToBounds = true
        
        self.autocorrectionType = UITextAutocorrectionType.no
        self.spellCheckingType = UITextSpellCheckingType.no
        self.autocapitalizationType = UITextAutocapitalizationType.allCharacters
        
        self.prepareTextField()
    }
    
    fileprivate var count : Int?
    fileprivate var limit : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if delegate == nil {
            delegate = self
        }
    }
    
    var borderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    var placeHolderColor: UIColor = UIColor.white.withAlphaComponent(0.5) {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
    
    override var placeholder: String? {
        didSet {
            attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes: [NSForegroundColorAttributeName: placeHolderColor])
            tintColor = placeHolderColor
        }
    }
    
    func setNumberFormatWithCount(_ count: Int, limit: Int) {
        self.textFormat = .Number
        self.count = count
        self.limit = limit
    }
    
    @IBInspectable var textFormat: TextFormat = TextFormat.Default
    func prepareTextField() {
        switch self.textFormat {
        case .Default:
            self.keyboardType = UIKeyboardType.default
        case .SocialSecurity:
            self.isSecureTextEntry = true
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
            self.count = 9
        case .ToothNumber:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
        case .Phone:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
        case .Zipcode:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
        case .Number:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
            if self.count == nil {
                self.count = 100
            }
        case .Date:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
            self.count = 2
            self.limit = 31
        case .MiddleInitial:
            self.keyboardType = UIKeyboardType.default
        case .Month:
            MonthListView.addMonthListForTextField(self)
        case .Year:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
            self.count = 4
        case .Time:
            DateInputView.addTimePickerForTextField(self)
        case .DateInCurrentYear:
            DateInputView.addDatePickerForTextField(self)
        case .DateIn1980:
            DateInputView.addDatePickerForDateOfBirthTextField(self)
        case .State:
            StateListView.addStateListForTextField(self)
        case .Amount:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
        case .ExtensionCode:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
        case .Email:
            self.keyboardType = UIKeyboardType.emailAddress
            self.autocapitalizationType = UITextAutocapitalizationType.none
        case .SecureText:
            self.isSecureTextEntry = true
        case .AlphaNumeric:
            self.keyboardType = UIKeyboardType.numbersAndPunctuation
        case .Username:
            self.keyboardType = UIKeyboardType.default
            self.autocapitalizationType = UITextAutocapitalizationType.none
        //use "setNumberFormatWithCount" function to validate other numeric values
        }
    }
}
extension MCTextField: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.textFormat == .Phone {
            return textField.formatPhoneNumber(range, string: string)
        } else if self.textFormat == .Zipcode {
            return textField.formatZipCode(range, string: string)
        } else if self.textFormat == .Number || self.textFormat == .SocialSecurity || self.textFormat == .Year || self.textFormat == .Date {
            return textField.formatNumbers(range, string: string, count: count!, limit: limit)
        } else if textFormat == .Amount {
            return textField.formatAmount(range, string: string)
        } else if textFormat == .ExtensionCode {
            return textField.formatExt(range, string: string)
        } else if self.textFormat == .MiddleInitial {
            return textField.formatMiddleName(range, string: string)
        } else if self.textFormat == .ToothNumber {
            return textField.formatToothNumbers(range, string: string)
        }
        return true
    }
}
