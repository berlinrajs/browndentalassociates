//
//  Extention.swift
//  WestgateSmiles
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


public let screenSize = UIScreen.main.bounds


public func getText(_ text : String) -> String {
    return "  \(text)  "
}

extension UIViewController {
    
    func showCustomAlert(_ message: String) {
        CustomAlert.alertView().showWithTitle(message) { 
            
        }
    }
    
    func showAlert(_ title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
            
        }
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func showAlert(_ message : String) {
        let alertController = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
            
        }
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func showAlert(_ message : String, completion: @escaping () -> Void) {
        let alertController = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
            completion()
        }
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func showAlert(_ message : String, buttonTitles: [String], completion: @escaping (_ buttonIndex: Int) -> Void) {
        let alertController = UIAlertController(title: kAppName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        for (idx, title) in buttonTitles.enumerated() {
            let alertOkAction = UIAlertAction(title: title.uppercased(), style: UIAlertActionStyle.destructive) { (action) -> Void in
                completion(idx)
            }
            alertController.addAction(alertOkAction)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
}


extension NSError {
    
    convenience init(errorMessage : String) {
        self.init(domain: "Error", code: 101, userInfo: [NSLocalizedDescriptionKey : errorMessage])
    }
    
}

extension UILabel {
    func setAttributedText() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        let attributedString = NSAttributedString(string: self.text!,
            attributes: [
                NSParagraphStyleAttributeName: paragraphStyle,
                NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float),
                NSFontAttributeName : self.font
            ])
        self.attributedText = attributedString
        
    }
    
    func compare(val1: String?, val2: String?) {
        if val1 != val2 {
            textColor = UIColor.red
        }
    }
}




extension UITextField {
    var isEmpty : Bool {
        return self.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lengthOfBytes(using: String.Encoding.utf8) == 0
    }
    
    func getText() -> String {
        if self.isEmpty{
            return ""
        }
        return self.text!
    }
    
    func setSavedText(text : String!) {
        if text == nil {
            self.text = ""
        }else{
            self.text = text
        }
    }

    
    func formatPhoneNumber(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        
        let components = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.characters.count
        let decimalStr = decimalString as NSString
        
        
        let hasLeadingOne = length > 0 && decimalStr.character(at: 0) == (1 as unichar)
        if length == 0 || (length > 10 && !hasLeadingOne) || length > 11
        {
            let newLength = self.text!.characters.count + string.characters.count - range.length as Int
            return (newLength > 10) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        if hasLeadingOne
        {
            formattedString.append("1 ")
            index += 1
        }
        if (length - index) > 3
        {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("(%@)", areaCode)
            index += 3
        }
        if length - index > 3
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", prefix)
            index += 3
        }
        
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        self.text = formattedString as String
        return false
    }
    
    func formatZipCode(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > 5 {
            return false
        }
        return true
    }
    
    func formatNumbers(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > 9 {
            return false
        }
        return true
    }
    
    func formatAmount(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string).replacingOccurrences(of: "$ ", with: "").replacingOccurrences(of: ",", with: "")
        
        self.setAmountText(newString)
        return false
    }
    
    func setAmountText(_ text: String) {
        var amountText = ""
        for (idx, char) in text.characters.reversed().enumerated() {
            if idx % 3 == 0 && idx != 0 {
                amountText = "," + amountText
            }
            amountText = "\(char)" + amountText
        }
        if !amountText.hasPrefix("$ ") && amountText.characters.count > 0 {
            amountText = "$ " + amountText
            self.text = amountText
        } else {
            self.text = amountText
        }
    }
    
    func formatExt(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > 3 {
            return false
        }
        return true
    }
    func formatMiddleName(_ range: NSRange, string: String) -> Bool {
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string).trimmingCharacters(in: CharacterSet.letters.inverted)
        if newString.characters.count > 1 {
            return false
        } else {
            text = newString
        }
        return false
    }
    
    func formatFamilyMembersCount(_ range: NSRange, string: String) -> Bool{
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > 2 {
            return false
        }
        return true
    }
    func formatNumbers(_ range: NSRange, string: String, count : Int, limit: Int?) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > count {
            return false
        }
        if limit != nil && limit > 0 {
            return Int(newString) <= limit
        }
        return true
    }
    
    
    func formatDate(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > 2 {
            return false
        }
        //        let maximumDateForMonth: [String: Int] = ["": 31, "JAN": 31, "FEB": 29, "MAR": 31 ,"APR": 30 ,"MAY": 31 ,"JUN": 30 ,"JUL": 31 ,"AUG": 31 ,"SEP": 30 ,"OCT": 31 ,"NOV": 30 ,"DEC": 31]
        if Int(newString) > 31 {
            return false
        }
        return true
    }
    

    func formatAmericanCreditCardNumber(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        
        let components = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.characters.count
        let decimalStr = decimalString as NSString
        
        
        if length == 0 || length > 15
        {
            let newLength = self.text!.characters.count + string.characters.count - range.length as Int
            return (newLength > 15) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        
        if (length - index) > 5
        {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 5))
            formattedString.appendFormat("%@ ", areaCode)
            index += 5
        }
        if length - index > 5
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 5))
            formattedString.appendFormat("%@ ", prefix)
            index += 5
        }
        if length - index > 5
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 5))
            formattedString.appendFormat("%@ ", prefix)
            index += 5
        }
        
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        self.text = formattedString as String
        return false
    }
    
    func formatCreditCardNumber(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        
        let components = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.characters.count
        let decimalStr = decimalString as NSString
        
        
        if length == 0 || length > 16
        {
            let newLength = self.text!.characters.count + string.characters.count - range.length as Int
            return (newLength > 16) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        
        if (length - index) > 4
        {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", areaCode)
            index += 4
        }
        if length - index > 4
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", prefix)
            index += 4
        }
        if length - index > 4
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", prefix)
            index += 4
        }
        
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        self.text = formattedString as String
        return false
    }

    
    
    func formatLicense(_ range: NSRange, string: String, count : Int) -> Bool {
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.characters.count > count {
            return false
        }
        return true
    }
    func formatToothNumbers(_ range: NSRange, string: String) -> Bool {
        if string.lengthOfBytes(using: String.Encoding.utf8) == 0 {
            return true
        }
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890,").inverted) != nil {
            return false
        }
        
        let newRange = self.text!.characters.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.characters.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let textFieldString = self.text!.replacingCharacters(in: newRange, with: string)
        let textString = textFieldString.components(separatedBy: ",")
        
        if textFieldString.characters.count > 2 {
            let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
            let lastTwoStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2))
            let lastThreeStrings = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 3))
            
            if lastTwoStrings == ",," {
                return false
            }
            if lastString == "," && lastThreeStrings.components(separatedBy: ",").count == 3 {
                let requiredString = textFieldString.substring(to: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 2)) + "0" + lastTwoStrings
                self.text = requiredString
                return false
            }
            
        } else {
            if textFieldString.characters.count == 2 {
                let lastString = textFieldString.substring(from: textFieldString.characters.index(textFieldString.startIndex, offsetBy: textFieldString.characters.count - 1))
                if lastString == "," {
                    self.text = "0" + textFieldString
                    return false
                }
            }
            if textFieldString == "," {
                return false
            }
        }
        for text in textString {
            if text == "0" {
                return true
            }
            if text == "00" {
                return false
            }
            if Int(text) > 35 {
                return false
            }
        }
        return true
    }
}

extension UITextView {
    var isEmpty : Bool {
        return self.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lengthOfBytes(using: String.Encoding.utf8) == 0
    }
}



extension String {
    
    var fileName : String {
        return self.replacingOccurrences(of: " - ", with: "_").replacingOccurrences(of: " ", with: "_").replacingOccurrences(of: "/", with: "_OR_").replacingOccurrences(of: "-", with: "_")
    }
    
    var formattedPhoneNumber: String {
        get {
            if self.characters.count != 10 {
                return self
            }
            var phone: String = "("
            for char in self.characters {
                phone.append(char)
                if phone.characters.count == 4 {
                    phone = phone + ")"
                }
                if phone.characters.count == 8 {
                    phone = phone + "-"
                }
            }
            return phone
        }
    }
    
    public func rangeOfText(_ text : String) -> NSRange {
        return NSMakeRange(self.characters.count - text.characters.count, text.characters.count)
    }
    
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.height
    }
    
    var isValidEmail : Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    var socialSecurityNumber: String {
        get {
            if self.characters.count != 9 {
                return self
            }
            var ssn: String = ""
            for char in self.characters {
                ssn.append(char)
                if ssn.characters.count == 3 || ssn.characters.count == 6 {
                    ssn = ssn + "-"
                }
            }
            return ssn
        }
    }
    
    var isValidYear: Bool {
        if self.characters.count != 4 {
            return false
        }
        let components = (Calendar.current as NSCalendar).components(NSCalendar.Unit.year, from: Date())
        if Int(self) > components.year {
            return false
        }
        return true
    }
    
    
    var isCreditCard: Bool {
        let charcter  = NSCharacterSet(charactersIn: "01234567890").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered.characters.count == 16
    }
    var isAmericanCreditCard: Bool {
        let charcter  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered.characters.count == 15
    }

    
    var isPhoneNumber: Bool {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered.characters.count == 10
    }
    var phoneNumber: String {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered
    }
    
    var isZipCode: Bool {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered.characters.count == 5
    }
    
    var isSocialSecurityNumber: Bool {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered.characters.count == 9
    }
    var isValidMRN: Bool {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered.characters.count >= 7 && filtered.characters.count <= 8
    }
    
    var isValidExt: Bool {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString:NSArray = self.components(separatedBy: charcter) as NSArray
        let filtered = inputString.componentsJoined(by: "")
        return filtered.characters.count == 3
    }
    
    func setTextForArrayOfLabels(_ arrayOfLabels: [UILabel]) {
        
        if arrayOfLabels.count == 0 {
            return
        }
        
        let wordArray = self.components(separatedBy: " ")
        
        var textToCheck: NSString = ""
        
        for string in wordArray {
            let previousLength = textToCheck.length
            let label = arrayOfLabels[0]
            textToCheck = textToCheck.length == 0 ? ((textToCheck as String) + string) as NSString: ((textToCheck as String) + " " + string) as NSString
            
            let size = textToCheck.size(attributes: [NSFontAttributeName: label.font])
            if size.height > label.frame.height || size.width > label.frame.width {
                var array = arrayOfLabels
                array.removeFirst()
                ((self as NSString).replacingCharacters(in: NSMakeRange(0, previousLength), with: "").trimmingCharacters(in: CharacterSet.whitespaces)).setTextForArrayOfLabels(array)
                return
            } else {
                label.text = textToCheck as String
            }
        }
    }
  
}

extension NSAttributedString {
    func heightWithConstrainedWidth(_ width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        return ceil(boundingBox.height) + 10
    }

}

